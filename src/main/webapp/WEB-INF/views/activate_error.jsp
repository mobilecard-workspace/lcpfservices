<%@ page contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<%@ page isELIgnored="false" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width" />
<style type="text/css">
root {
	display: block;
}

html {
	margin: 0;
	font-size: 62.5%;
	color: white;
	background-color: #ffffff;
}
body {
	font-size: 14px;
    font-weight: bold;
	line-height: 1.428571429;
}

h2{
	 text-align: center;
}

div#envelope{  /* #D3441C para desktop*/
			width:40%;
			margin: 0 auto;
			background-color:#ffffff;
			padding:10px 0;
			/*border:1px solid gray;
			border-radius:10px;*/
			COLOR: #000000;
			/*background-image: url("../images/back1.png");
			background-size: cover;
			background-position: center;*/
		}
		 
		@media only screen and (max-width: 768px) {
			div#envelope{
			width:98%;  /* para phones */
			margin: 0 auto;
			background-color:#ffffff;
			padding:10px 0;
			/*border:1px solid gray;
			border-radius:10px;*/
			COLOR: #000000;
			/*background-image: url("../images/back1.png");
			background-size: cover;
			background-position: center;*/
		}
}

 
</style>
<title>La cuenta por favor</title>
</head>
<body>
	<h3>${message}</h3>
</body>
</html>