<%@ page contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width" />
<title>Redirección Payworks</title>
<style type="text/css">
root {
	display: block;
}

html {
	margin: 0;
	font-size: 62.5%;
	
}
body {
	font-size: 14px;
    font-weight: bold;
	line-height: 1.428571429;
}

@media ( min-width :750px) {
	.container {
		width: 100%;
		margin-right: auto;
		margin-left: auto;
	}
}

@media ( min-width :992px) {
	.container {
		width: 700px;
		margin-right: auto;
		margin-left: auto;
	}
} 
</style>

<script type="text/javascript">
	function sendform() {
		if (self.name.length === 0) {
			self.name = "gotoProsa";
		}
		//var twnd = window.open("","wnd","toolbar=0,location=0,directories=0,status=0,menubar=0,resizable=1,copyhistory=0,width=760,height=750");
		//document.form1.return_target.value = self.name.toString();
		//document.form1.target = "wnd";
		document.form1.submit();
	}
</script>
</head>
<body onload="sendform();">
	<form method="post" name="form1" action="https://via.banorte.com/payw2">
		<input type="hidden" name="ID_AFILIACION" value="${payworks.ID_AFILIACION}" /> 
		<input type="hidden" name="USUARIO" value="${payworks.USUARIO}" /> 
		<input type="hidden" name="CLAVE_USR" value="${payworks.CLAVE_USR}" /> 
		<input type="hidden" name="CMD_TRANS" value="${payworks.CMD_TRANS}" /> 
		<input type="hidden" name="ID_TERMINAL" value="${payworks.ID_TERMINAL}" /> 
		<input type="hidden" name="MONTO" value="${payworks.Total}" /> 
		<input type="hidden" name="MODO" value="${payworks.MODO}" /> 
		<%-- <input type="hidden" name="REFERENCIA" value="${prosa.referencia}" />  --%>
		<input type="hidden" name="NUMERO_CONTROL" value="${payworks.Reference3D}" /> 
		<%-- <input type="hidden" name="REF_CLIENTE1" value="${prosa.REF_CLIENTE1}" /> 
		<input type="hidden" name="REF_CLIENTE2" value="${prosa.REF_CLIENTE2}" /> 
		<input type="hidden" name="REF_CLIENTE3" value="${prosa.REF_CLIENTE3}" /> 
		<input type="hidden" name="REF_CLIENTE4" value="${prosa.REF_CLIENTE4}" /> 
		<input type="hidden" name="REF_CLIENTE5" value="${prosa.REF_CLIENTE5}" /> --%> 
		<input type="hidden" name="NUMERO_TARJETA" value="${payworks.Number}" /> 
		<input type="hidden" name="FECHA_EXP" value="${payworks.FECHA_EXP}" /> 
		<input type="hidden" name="CODIGO_SEGURIDAD" value="${payworks.CODIGO_SEGURIDAD}" /> 
		<%-- <input type="hidden" name="CODIGO_AUT" value="${prosa.auth}" /> --%> 
		<input type="hidden" name="MODO_ENTRADA" value="${payworks.MODO_ENTRADA}" />
		<%-- <input type="hidden" name="LOTE" value="${prosa.lote}" /> --%>
		<input type="hidden" name="URL_RESPUESTA" value="${payworks.URL_RESPUESTA}" />
		<input type="hidden" name="IDIOMA_RESPUESTA" value="${payworks.IDIOMA_RESPUESTA}" />
		<%
		    String xid = request.getParameter("XID");
			String cavv = request.getParameter("CAVV");
		    if (xid != null || xid != "" && cavv != null || cavv != "") {
		%>
		    <input type="hidden" name="XID" value="${payworks.XID}" />
			<input type="hidden" name="CAVV" value="${payworks.CAVV}" />
		<%
		    }
		%>
		
		
		<input type="hidden" name="ESTATUS_3D" value="${payworks.Status}" />
		<input type="hidden" name="ECI" value="${payworks.ECI}" />
	</form>
	<div class="container">
		
		<br />
		<table border="0" width="100%">
			<thead>
			<tr class="cart_menu">
				<th class="description"><p>Envio de informaci&oacute;n.</p></th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td class="cart_description">
					<p><br/>Se est&aacute; enviando la informaci&oacute;n, favor de esperar. <br/>
					</p>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
</body>
</html>