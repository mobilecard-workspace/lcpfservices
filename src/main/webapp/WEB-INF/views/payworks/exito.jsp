<%@ page contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>

<head>
    <title>Transacci&oacute;n declinada</title>
    <!-- Meta Tags -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="expires" content="-1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
%>
    	<style>
	    * {
		  /*with these codes padding and border does not increase it's width.Gives intuitive style.*/
		  -webkit-box-sizing: border-box;   
		  -moz-box-sizing: border-box;
		  box-sizing: border-box;
		  color:#33313;
		  font-family: "Arial", Helvetica, sans-serif;
		}

		body {
		   margin:0;
		   padding:0;		  
		   background-color:#8C8C8C;
		   color: white;  
		}
		div#envelope{
			width:90%;
			margin: 0 auto;
			background-color:#D3441C;
			padding:10px 0;
			border:1px solid gray;
			border-radius:10px;
		} 
		table{
			width:80%;
			margin:0 10%;
		}  
		table header {
		  text-align:center;
		  font-family: 'Roboto Slab', serif;
		}
		
	</style>
</head>
<body>
<div id="envelope">

<table class="table ">
	<thead>
		<tr class="cart_menu">
			<th class="description" colspan="2">Resultado de la Transacci&oacute;n: Exitosa</th>
		</tr>
	</thead>
	<tbody>
		<!--  <tr>
			<td><p>${mensajeError}</p></td>
		</tr>
		 -->
		 ${mensajeError}
		<tr>
			<td colspan="2"><BR>
			<BR>Presione el boton atras para terminar.</td>
		</tr>
	</tbody>
</table>

</div>
</body>
</html>