<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>

<head>
<title>Purchase Verification</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width" />
</script>
<style type="text/css">
root {
	display: block;
}

html {
	margin: 0;
	font-size: 62.5%;
}
body {
	font-size: 14px;
    font-weight: bold;
	line-height: 1.428571429;
}

@media ( min-width :750px) {
	.container {
		width: 100%;
		margin-right: auto;
		margin-left: auto;
	}
}

@media ( min-width :992px) {
	.container {
		width: 700px;
		margin-right: auto;
		margin-left: auto;
	}
} 
</style>
</head>
<body style="width: 100%">
<form:form name="form1" autocomplete="off" action="https://eps.banorte.com/secure3d/Solucion3DSecure.htm" modelAttribute="payworks">
<form:hidden path="Reference3D"/>
<form:hidden path="MerchantId"/>
<input type="hidden" name="MerchantName" value="LCPF" />
<input type="hidden" name="MerchantCity" value="Monterrey" />
<input type="hidden" name="Cert3D" value="03" />
<input type="hidden" name="NoFrame" value="True" />
<form:hidden path="ForwardPath"/>
<form:hidden path="Expires"/>
<form:hidden path="Total"/>
<form:hidden path="Card"/>
<form:hidden path="CardType"/>
</form:form>

	<div class="container">
		
		<br />
		<table border="0" width="100%">
			<thead>
			<tr>
				<th><p>Envio de informaci&oacute;n.</p></th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>
					<p><br/>Se est&aacute; enviando la informaci&oacute;n, favor de esperar. <br/>
					</p>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
	<script type="text/javascript">
		document.form1.submit();
	</script>
</body>
</html>
