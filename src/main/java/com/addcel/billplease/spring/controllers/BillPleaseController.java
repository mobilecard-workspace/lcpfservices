package com.addcel.billplease.spring.controllers;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.billplease.mybatis.model.vo.Establecimiento;
import com.addcel.billplease.mybatis.model.vo.Operador;
import com.addcel.billplease.spring.model.request.Login;
import com.addcel.billplease.spring.model.request.LoginAdmin;
import com.addcel.billplease.spring.model.request.LoginOper;
import com.addcel.billplease.spring.model.request.getEstRequest;
import com.addcel.billplease.spring.model.response.AddOpResponse;
import com.addcel.billplease.spring.model.response.BankCodesResponse;
import com.addcel.billplease.spring.model.response.BaseResponse;
import com.addcel.billplease.spring.model.response.CuentasEstResponse;
import com.addcel.billplease.spring.model.response.LoginResponse;
import com.addcel.billplease.spring.model.response.MCSignUpResponse;
import com.addcel.billplease.spring.model.response.MCTransafersResponse;
import com.addcel.billplease.spring.model.response.VerifyResponse;
import com.addcel.billplease.spring.services.BillPleaseServices;


@RestController
public class BillPleaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BillPleaseController.class);
	
	@Autowired
	BillPleaseServices serviceBill;
	
	@Autowired
    private ServletContext servletContext;
	
	@RequestMapping(value = "/test",method = RequestMethod.GET)
	public ModelAndView ejemplosend(){
		ModelAndView view = new ModelAndView("index");
		
		return view;
	}
	
	
	
	@RequestMapping(value = "/operador/login/v2/",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public LoginResponse  OperLogin(@RequestBody LoginOper login){
		
		return serviceBill.OperLogin(login);
		
	}
	@RequestMapping(value = "/operador/login",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public LoginResponse  OperLogin(@RequestBody Login login){
		
		return serviceBill.OperLogin(login);
		
	}
	 
	@RequestMapping(value = "/admin/login/v2/",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public LoginResponse AdminLogin(@RequestBody LoginAdmin login){
			
			return serviceBill.EstabLogin(login);
		}
	@RequestMapping(value = "/admin/login",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public LoginResponse AdminLogin(@RequestBody Login login){
			
			return serviceBill.EstabLogin(login);
		}
	 
	@RequestMapping(value = "/admin/{idioma}/add",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public BaseResponse AdminAdd(@RequestBody Establecimiento est, @PathVariable String idioma){
			
			return serviceBill.AltaEstablecimiento(est, idioma);
		}
	
	@RequestMapping(value = "/admin/{idioma}/update",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public BaseResponse AdminUpdate(@RequestBody Establecimiento est, @PathVariable String idioma){
			
			return serviceBill.AdminUpdate(est, idioma);
		}
	
	//ACTUALIAZAR PASS DE ADMIN
	@RequestMapping(value = "/admin/{idioma}/passUpdate",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public BaseResponse AdminUpdatePass(@PathVariable String idioma, @RequestParam("userAdmin") long idAdmin,  @RequestParam("newPass") String newPass){
			
			return serviceBill.adminPassUpdate(idioma, idAdmin, newPass);
		}
	//ACTUALIAZAR PASS DE OPERADOR-ADMIN
		@RequestMapping(value = "/admin/{idioma}/operador/passUpdate",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public BaseResponse AdminUpdateOperadorPass(@PathVariable String idioma, @RequestParam("userAdmin") long idAdmin, @RequestParam("idOperador") long idOperador , @RequestParam("newPass") String newPass){
				
			return serviceBill.updateOperadorPass(idioma,idAdmin,idOperador,newPass);
		}
	
	//RESETEAR PASS ADMIN
	@RequestMapping(value = "/admin/{idioma}/passReset",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public BaseResponse AdminResetPass(@PathVariable String idioma, @RequestParam("userAdmin") String idAdmin){
			
			return serviceBill.AdminResetPass(idioma, idAdmin);
		}
	
	//RESETEAR PASS OPERADOR-ADMIN
		@RequestMapping(value = "/admin/{idioma}/operador/passReset",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
		public BaseResponse AdminOperadorResetPass(@PathVariable String idioma, @RequestParam("userAdmin") long idAdmin, @RequestParam("idOperador") long idOperador){
				
				return serviceBill.AdminResetPassOperador(idioma,idAdmin,idOperador);
			}
	
	
	@RequestMapping(value = "/admin/{idioma}/delete",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public BaseResponse AdminDelete(@RequestParam long id, @PathVariable String idioma){
			
			return serviceBill.AdminDelete(id, idioma);
		}
	
	@RequestMapping(value = "/operador/{idioma}/add",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public BaseResponse OperadorAdd(@RequestBody Operador op, @PathVariable String idioma){
			
			return serviceBill.AltaOperador(op, idioma);
		}
	
	@RequestMapping(value = "/operador/{idioma}/update",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public AddOpResponse OperUpdate(@RequestBody Operador oper, @PathVariable String idioma){
			
			return serviceBill.updateOperador(oper, idioma);
		}
	
	/*@RequestMapping(value = "/operador/{idioma}/delete",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public BaseResponse OperDelete(@RequestParam long id_est, @RequestParam long id_oper ,@PathVariable String idioma){
			
			return serviceBill.OperDelete(id_est, id_oper, idioma);
		}*/
	@RequestMapping(value = "{id_est}/{idioma}/operador/delete",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public AddOpResponse OperDelete(@PathVariable long id_est, @RequestParam long idOperador ,@PathVariable String idioma){
			
			return serviceBill.OperDelete(id_est, idOperador, idioma);
		}
	
	
	@RequestMapping(value = "/admin/{idioma}/{id_est}/operadores",method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public AddOpResponse getOperadores(@PathVariable String idioma, @PathVariable long id_est){
			
			return serviceBill.getOperadores(id_est, idioma);
		}
	 
	@RequestMapping(value = "/bankCodes",method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public BankCodesResponse getBankCodes(){
		
		return serviceBill.getBankCodes();
	}
	
	/**ACTIVAR CUENTA DE USUARIO Establecimiento**/
	@RequestMapping(value = "{idioma}/user/activate", method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
	public ModelAndView Useractivate(@PathVariable String idioma, @RequestParam("codigo") String cadena){
		
		return serviceBill.userActivate(cadena, idioma);
	}
	
	//verificar cuenta usuario
	@RequestMapping(value = "{idioma}/{type}/verify", method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public VerifyResponse userVerify(@PathVariable String idioma, @RequestParam("id_usuario") long id_usuario, @PathVariable int type){
		return serviceBill.UserVerify(id_usuario, type, idioma);
	}
	
	//obtener establecimientos
	@RequestMapping(value = "{idioma}/getEstablecimientos", method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public MCSignUpResponse geFullEstablecimientos(@PathVariable String idioma, @RequestBody getEstRequest request){
		return serviceBill.getFullestablecimientos(idioma, request);
	}
	
	
	//obtener cuentas de establecinmientos
	@RequestMapping(value = "{idioma}/admin/{id}/cuentas", method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
	public CuentasEstResponse getCuentasEstablecimientos(@PathVariable String idioma,  @PathVariable long id){
		return serviceBill.getCuentasEstablecimiento(idioma, id);
	}
	
	//actualizar mesero-cuenta
	@RequestMapping(value = "{idioma}/setOperador", method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public MCTransafersResponse setOperador(@PathVariable String idioma,  @RequestParam("idOperador") long idOperador,@RequestParam("idBitacora") long idBitacora){
		return serviceBill.setOperador(idOperador, idBitacora, idioma);
	}
	
	  @RequestMapping(value = "/logo_default.png", method = RequestMethod.GET)
	    public void getImageAsByteArray(HttpServletResponse response) throws IOException {
		  LOGGER.debug("PETICION IMAGEN LOGO");
	        final InputStream in = servletContext.getResourceAsStream("/WEB-INF/images/logo_default.png");
	        response.setContentType(MediaType.IMAGE_PNG_VALUE); //IMAGE_JPEG_VALUE
	        IOUtils.copy(in, response.getOutputStream());
	    }
	
}
