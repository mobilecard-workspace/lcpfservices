package com.addcel.billplease.spring.controllers;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import com.addcel.billplease.spring.services.ReportServices;

@RestController
public class ReportController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);
	
	@Autowired
	private ReportServices reports;
	

	@RequestMapping(value = "reportLCPF", method = RequestMethod.GET)
    public ModelAndView getDocuments(Model model,HttpServletRequest request) {
		LOGGER.debug("iniciando reporte");
		String fecha = request.getParameter("fecha");
        model.addAttribute("courses", reports.getEstablecimientos());
		model.addAttribute("fecha", fecha);
		LOGGER.debug("fecha: " + fecha);
		ModelAndView view = new ModelAndView("reports/LCPFReport",model.asMap());
		
		return view;
        //return new ModelAndView("reports/LCPFReport","courses",reports.getEstablecimientos());
              
    }
	
}
