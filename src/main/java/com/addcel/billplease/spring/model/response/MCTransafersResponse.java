package com.addcel.billplease.spring.model.response;

public class MCTransafersResponse extends BaseResponse {

	private double montoTransfer;
	private long idBitacora;
	private double comision;
	private String authProcom;
	private String referenceBanorte;
	private String refProcom;
	private String tarjeta;
	private String fecha;
	private String referenciaNeg;
	
	
	public MCTransafersResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}
	
	public long getIdBitacora() {
		return idBitacora;
	}
	
	public void setReferenciaNeg(String referenciaNeg) {
		this.referenciaNeg = referenciaNeg;
	}
	
	public String getReferenciaNeg() {
		return referenciaNeg;
	}

	public void setRefProcom(String refProcom) {
		this.refProcom = refProcom;
	}
	
	public String getRefProcom() {
		return refProcom;
	}
	
	public double getMontoTransfer() {
		return montoTransfer;
	}

	public void setMontoTransfer(double montoTransfer) {
		this.montoTransfer = montoTransfer;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public String getAuthProcom() {
		return authProcom;
	}

	public void setAuthProcom(String authProcom) {
		this.authProcom = authProcom;
	}

	public String getReferenceBanorte() {
		return referenceBanorte;
	}

	public void setReferenceBanorte(String referenceBanorte) {
		this.referenceBanorte = referenceBanorte;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	
}
