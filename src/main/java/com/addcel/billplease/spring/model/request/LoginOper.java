package com.addcel.billplease.spring.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginOper {

	private long id_establecimiento;
	private String usuario;
	private String pass;
	private String idioma;
	
	public LoginOper() {
		// TODO Auto-generated constructor stub
	}

	public long getId_establecimiento() {
		return id_establecimiento;
	}

	public void setId_establecimiento(long id_establecimiento) {
		this.id_establecimiento = id_establecimiento;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}
	
	
}
