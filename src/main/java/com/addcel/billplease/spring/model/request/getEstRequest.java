package com.addcel.billplease.spring.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class getEstRequest {

	private long lat;
	private long lon;
	private String ubicacion_actual;
	
	public getEstRequest() {
		// TODO Auto-generated constructor stub
	}
	
	public long getLat() {
	return lat;
}



public void setLat(long lat) {
	this.lat = lat;
}



public long getLon() {
	return lon;
}



public void setLon(long lon) {
	this.lon = lon;
}



public String getUbicacion_actual() {
	return ubicacion_actual;
}



public void setUbicacion_actual(String ubicacion_actual) {
	this.ubicacion_actual = ubicacion_actual;
}
}
