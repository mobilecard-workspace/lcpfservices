package com.addcel.billplease.spring.model.request;

public class PayworksVO {

	private String Reference3D;
	private String MerchantId;
	private String ForwardPath;
	private String Expires;
	private String Total;
	private String Card;
	private String CardType;
	
	public PayworksVO() {
		// TODO Auto-generated constructor stub
	}

	public String getReference3D() {
		return Reference3D;
	}

	public void setReference3D(String reference3d) {
		Reference3D = reference3d;
	}

	public String getMerchantId() {
		return MerchantId;
	}

	public void setMerchantId(String merchantId) {
		MerchantId = merchantId;
	}

	public String getForwardPath() {
		return ForwardPath;
	}

	public void setForwardPath(String forwardPath) {
		ForwardPath = forwardPath;
	}

	public String getExpires() {
		return Expires;
	}

	public void setExpires(String expires) {
		Expires = expires;
	}

	public String getTotal() {
		return Total;
	}

	public void setTotal(String total) {
		Total = total;
	}

	public String getCard() {
		return Card;
	}

	public void setCard(String card) {
		Card = card;
	}

	public String getCardType() {
		return CardType;
	}

	public void setCardType(String cardType) {
		CardType = cardType;
	}
	
	
}
