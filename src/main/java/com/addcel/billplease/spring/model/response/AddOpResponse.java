package com.addcel.billplease.spring.model.response;

import java.util.List;

import com.addcel.billplease.mybatis.model.vo.Operador;

public class AddOpResponse  extends BaseResponse{

	private List<Operador> operadores;
	
	public AddOpResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public void setOperadores(List<Operador> operadores) {
		this.operadores = operadores;
	}
	
	public List<Operador> getOperadores() {
		return operadores;
	}
}
