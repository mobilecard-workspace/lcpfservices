package com.addcel.billplease.spring.model.response;

import java.util.List;

import com.addcel.billplease.mybatis.model.vo.BankCodes;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BankCodesResponse extends BaseResponse{

	private List<BankCodes> banks;
	
	public BankCodesResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public void setBanks(List<BankCodes> banks) {
		this.banks = banks;
	}
	
	public List<BankCodes> getBanks() {
		return banks;
	}
	
}
