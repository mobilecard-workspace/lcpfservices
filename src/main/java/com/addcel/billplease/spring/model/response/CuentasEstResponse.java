package com.addcel.billplease.spring.model.response;

import java.util.List;

import com.addcel.billplease.mybatis.model.vo.CuentasEstablecimiento;

public class CuentasEstResponse extends BaseResponse {

	List<CuentasEstablecimiento> cuentas;
	
	public CuentasEstResponse() {
		// TODO Auto-generated constructor stub
	}
	public void setCuentas(List<CuentasEstablecimiento> cuentas) {
		this.cuentas = cuentas;
	}
	public List<CuentasEstablecimiento> getCuentas() {
		return cuentas;
	}
}
