package com.addcel.billplease.spring.model.response;

import java.util.ArrayList;
import java.util.List;

import com.addcel.billplease.mybatis.model.vo.Establecimiento;
import com.addcel.billplease.mybatis.model.vo.FromAccount;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MCSignUpResponse extends BaseResponse{

	private List<FromAccount> accounts;
	
	public MCSignUpResponse() {
		// TODO Auto-generated constructor stub
		this.accounts = new ArrayList<FromAccount>();
	}



	public void setAccounts(List<FromAccount> list) {
		this.accounts = list;
	}
	
	public List<FromAccount> getAccounts() {
		return accounts;
	}
	
}
