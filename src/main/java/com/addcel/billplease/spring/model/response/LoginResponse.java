package com.addcel.billplease.spring.model.response;

public class LoginResponse extends BaseResponse{

	private long id;
	private int status;
	private String nombre_establecimiento;
	private String direccion_establecimiento;
	private long id_establecimiento;
	
	public LoginResponse() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public String getNombre_establecimiento() {
		return nombre_establecimiento;
	}



	public void setNombre_establecimiento(String nombre_establecimiento) {
		this.nombre_establecimiento = nombre_establecimiento;
	}



	public String getDireccion_establecimiento() {
		return direccion_establecimiento;
	}



	public void setDireccion_establecimiento(String direccion_establecimiento) {
		this.direccion_establecimiento = direccion_establecimiento;
	}



	public long getId_establecimiento() {
		return id_establecimiento;
	}



	public void setId_establecimiento(long id_establecimiento) {
		this.id_establecimiento = id_establecimiento;
	}



	public void setStatus(int status) {
		this.status = status;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}
	
}
