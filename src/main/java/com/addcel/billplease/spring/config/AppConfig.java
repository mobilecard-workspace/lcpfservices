package com.addcel.billplease.spring.config;

import java.security.KeyStore;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

import com.addcel.billplease.spring.report.views.XlsView;
import com.addcel.billplease.spring.report.views.XlsxStreamingView;
import com.addcel.billplease.spring.report.views.XlsxView;

import javax.sql.DataSource;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.addcel.billplease.spring")
@MapperScan("com.addcel.billplease.mybatis.model.mapper")
@PropertySource("classpath:properties/datasource.properties")
public class AppConfig extends WebMvcConfigurerAdapter{

	public static final X509HostnameVerifier ALLOW_ALL_HOSTNAME_VERIFIER     = new AllowAllHostnameVerifier();
	
	
	/**
	 * Valores recuperados de datasource.properties
	 */
	@Value( "${jdbc.url}" ) private String jdbcUrl;
    @Value( "${jdbc.driverClassName}" ) private String driverClassName;
    @Value( "${jdbc.username}" ) private String username;
    @Value( "${jdbc.password}" ) private String password;
    
	
     /**
      * Crear Datasource para la conexion
      * @return DataSource
      */
	 public DataSource dataSource() {
       SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        dataSource.setDriverClass(com.mysql.jdbc.Driver.class);
        dataSource.setUsername(username);
        dataSource.setUrl(jdbcUrl);
        dataSource.setPassword(password);
       
		/*PooledDataSource dataSource = new PooledDataSource();
        dataSource.setDriver(driverClassName);
        dataSource.setUsername(username);
        dataSource.setUrl(jdbcUrl);
        dataSource.setPassword(password);
        
        dataSource.setPoolMaximumActiveConnections(10);
        dataSource.setPoolMaximumIdleConnections(5);
        dataSource.setPoolMaximumCheckoutTime(100);
        dataSource.setPoolPingQuery("select 1");*/
        
        return dataSource;
    }
	 
   
    
	
	  /**
	   *  Crear DataSourceTransactionManager a partir del datasource
	   * @return DataSourceTransactionManager
	   */
	  @Bean
       public DataSourceTransactionManager transactionManager()
	    {
	        return new DataSourceTransactionManager(dataSource());
	    }
	     /**
	      * Crear sessionFactory
	      * @return SqlSessionFactoryBean
	      * @throws Exception
	      */
	    @Bean
	    public SqlSessionFactoryBean sqlSessionFactoryBean() throws Exception{
	        SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
	        sessionFactory.setDataSource(dataSource());        
	        // mybatis mapper
	        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/*.xml"));
	        sessionFactory.setTypeAliasesPackage("com.addcel.billplease.mybatis.model.vo");
	        
	        return sessionFactory;
	    }
	    
	    
	  /*  @Override
	    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
	        configurer
	                .defaultContentType(MediaType.TEXT_HTML)
	                .parameterName("type")
	                .favorParameter(true)
	                .ignoreUnknownPathExtensions(false)
	                .ignoreAcceptHeader(false)
	                .useJaf(true);
	    }*/
	    
	   /* @Override
	    public void configureViewResolvers(ViewResolverRegistry registry) {
	        registry.jsp("/WEB-INF/views/", ".jsp");
	        registry.enableContentNegotiation(
	                new XlsView(),
	                new XlsxView(),
	                new XlsxStreamingView());
	    }*/
	    
	    @Bean
	    public InternalResourceViewResolver jspViewResolver() {
	        InternalResourceViewResolver bean = new InternalResourceViewResolver();
	        bean.setPrefix("/WEB-INF/views/");
	        bean.setSuffix(".jsp");
	        return bean;
	    }
	    
	  
	    
	    
	    
	    
}
