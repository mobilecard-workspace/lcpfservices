package com.addcel.billplease.spring.services;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.billplease.mybatis.model.mapper.ServiceMapper;
import com.addcel.billplease.mybatis.model.vo.AfiliacionVO;
import com.addcel.billplease.mybatis.model.vo.Card;
import com.addcel.billplease.mybatis.model.vo.Establecimiento;
import com.addcel.billplease.mybatis.model.vo.ProjectMC;
import com.addcel.billplease.mybatis.model.vo.Proveedor;
import com.addcel.billplease.mybatis.model.vo.Rules_LCPF;
import com.addcel.billplease.mybatis.model.vo.TBitacoraBanorte;
import com.addcel.billplease.mybatis.model.vo.TBitacoraVO;
import com.addcel.billplease.mybatis.model.vo.User;
import com.addcel.billplease.spring.model.request.PaymentRequest;
import com.addcel.billplease.spring.model.request.PayworksVO;
import com.addcel.billplease.spring.model.response.BaseResponse;
import com.addcel.billplease.spring.model.response.MCTransafersResponse;
import com.addcel.billplease.utils.Business;
import com.addcel.billplease.utils.Constantes;
import com.addcel.billplease.client.model.AccountRecord;
//import com.addcel.billplease.client.model.AccountRecord;
import com.addcel.billplease.client.model.EnqueuePaymentRequest;
import com.addcel.billplease.client.model.EnqueueTransactionResponse;
import com.addcel.billplease.client.model.ServiceResult;
import com.addcel.billplease.spring.services.H2HCLIENT;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;






@Service
public class PayworksService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PayworksService.class);
	
	@Autowired
	ServiceMapper mapper;
	
	private H2HCLIENT h2hclient = new H2HCLIENT();
	private SimpleDateFormat DFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");;
	
	private Gson gson = new Gson();
	
	
	public ModelAndView send3DSBanorte(PaymentRequest request, String referenciaNeg, double propina, long id_establecimiento){
		ModelAndView view = new ModelAndView();
		AfiliacionVO afiliacion = null;
		try{
		
			afiliacion = mapper.buscaAfiliacion("BANORTEH2HLCPF");
			Proveedor proveedor = mapper.getProveedor("H2HBanorteLCPF");
			
			Card card = mapper.getCard(request.getIdUser(), request.getIdCard()); //
			Establecimiento est = mapper.getEstablecimiento(id_establecimiento);
			request.setAccountId(est.getId_account());
			AccountRecord toAccount = mapper.getAccount(request.getAccountId());
			
			/**************************************************************
			 * hack para cobros extras. cobro del iva de la comision
			 */
			//Double comision_extra = mapper.getComisionExtra(request.getAmount(), request.getComision(), propina, request.getIdUser());
			//request.setComision(request.getComision()+comision_extra.doubleValue());
			/***************************************************************************/
			
			
			double comision = request.getComision();
			BigDecimal bd = new BigDecimal(comision);
			bd = bd.setScale(2, RoundingMode.HALF_DOWN);
			comision = bd.doubleValue();
			String numcard = UtilsService.getSMS(card.getNumerotarjeta());
			
			String ruleDB = mapper.Rules_LCPF(request.getIdUser(), numcard.substring(0,6), request.getIdCard(), request.getIdioma());
			Rules_LCPF rules = gson.fromJson(ruleDB, Rules_LCPF.class); 
			
			TBitacoraVO bitacora = new TBitacoraVO();
			bitacora.setTipo("");
			bitacora.setSoftware("");
			bitacora.setModelo("");
			bitacora.setWkey("");
			bitacora.setIdUsuario(request.getIdUser()+"");
			bitacora.setIdProducto(1);
			bitacora.setCar_id(1);
			bitacora.setCargo(request.getAmount()+comision+propina);
			bitacora.setIdProveedor(proveedor.getId_proveedor().intValue());
			bitacora.setConcepto("Transferencia H2HBANORTE: Concepto: " + request.getConcept() + " Monto: "
					+ request.getAmount() + "Propina: "+ propina + " Comision: " + comision );
			bitacora.setImei("");
			bitacora.setDestino( request.getAccountId()+"" );
			bitacora.setTarjeta_compra(card.getNumerotarjeta());
			bitacora.setAfiliacion(afiliacion.getIdAfiliacion());
			bitacora.setComision(comision);
			bitacora.setCx("");
			bitacora.setCy("");
			bitacora.setPin("");
			mapper.addBitacora(bitacora);
			
			TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
			bitacora_banorte.setAccount_id(request.getAccountId());
			bitacora_banorte.setAmount(request.getAmount());
			bitacora_banorte.setId_bitacora(bitacora.getIdBitacora());
			bitacora_banorte.setId_card(request.getIdCard());
			bitacora_banorte.setId_usuario(request.getIdUser());
			bitacora_banorte.setIdioma(request.getIdioma());
			bitacora_banorte.setTransaction_number(0);
			bitacora_banorte.setBank_reference("");
			bitacora_banorte.setComision(comision);
			bitacora_banorte.setTexto(rules!= null ? rules.getMessageError() : "");
			bitacora_banorte.setReferencia("");
			bitacora_banorte.setResultado_payw("");
			bitacora_banorte.setResultado_aut("");
			bitacora_banorte.setBanco_emisor("");
			bitacora_banorte.setTipo_tarjeta("");
			bitacora_banorte.setMarca_tarjeta("");
			bitacora_banorte.setCodigo_aut("");
			bitacora_banorte.setStatus3DS("");
			bitacora_banorte.setConcepto(request.getConcept());
			bitacora_banorte.setTarjeta_transferencia(card.getNumerotarjeta());
			bitacora_banorte.setNombre_destino(toAccount.getHolderName());
			bitacora_banorte.setCuenta_clabe_destino(toAccount.getActNumber());
			bitacora_banorte.setBanco_destino(toAccount.getBankCode());
			bitacora_banorte.setReferencia_negocio(referenciaNeg);
			bitacora_banorte.setPropina(propina);
			bitacora_banorte.setId_establecimiento(est.getId());
			//insertando en bitacora McBanorte
			mapper.insertTbitacoraMCBanorte(bitacora_banorte);
			
			
			if(rules.getErrorCode() != 0 || proveedor.getPrv_status() == 0){
				//new ModelAndView("payworks/pagina-error");
				
				MCTransafersResponse transfersresponse = new MCTransafersResponse();
				transfersresponse.setIdError(rules.getErrorCode());
				
				if(proveedor.getPrv_status() == 0){
					if(request.getIdioma().equalsIgnoreCase("es"))
						transfersresponse.setMensajeError("Por el momento el servicio no esta disponible.");
					else
						transfersresponse.setMensajeError("At the moment the service is not available.");
				}
				else
					transfersresponse.setMensajeError(rules.getMessageError());
				
				transfersresponse.setAuthProcom("");
				transfersresponse.setComision(0);
				transfersresponse.setFecha("");
				transfersresponse.setIdBitacora(bitacora.getIdBitacora());
				transfersresponse.setMontoTransfer(0);
				transfersresponse.setReferenceBanorte("");
				transfersresponse.setReferenciaNeg(referenciaNeg);
				transfersresponse.setRefProcom("");
				transfersresponse.setTarjeta("");
				
				
				//view.setViewName("transfers_response");
				//view.getModel().put("json", gson.toJson(transfersresponse));
				
				view = new ModelAndView("payworks/redirect_response");
				view.addObject("json", gson.toJson(transfersresponse));
				view.addObject("server",afiliacion.getForwardPath().split(" ")[0].split("/LCPF")[0]);
				return view;
			}
			
			double total = comision + request.getAmount() + propina;
			String cardType = card.getIdfranquicia() == 1? "VISA" : "MC";
			
			String vig = UtilsService.getSMS(card.getVigencia());
			String ct = UtilsService.getSMS(card.getCt()); 
			
			PayworksVO payVO = new PayworksVO();
			payVO.setCard(numcard);//  numcard 4152313189416436
			payVO.setCardType(cardType);//cardType VISA
			payVO.setExpires(vig);//vig "04/21"
			payVO.setForwardPath(afiliacion.getForwardPath().split(" ")[0]);//Constantes.URLBACK3DSPAYW
			payVO.setMerchantId(afiliacion.getMerchantId());
			payVO.setReference3D(bitacora.getIdBitacora()+"");
			payVO.setTotal(Business.formatoMontoPayworks(total+""));
			
			LOGGER.info("ENVIO DE INFO AL 3DS BANORTE"+"["+"]"+"["+bitacora.getIdBitacora()+"] " ); //+ gson.toJson(payVO)
			
			view.setViewName("payworks/comercio-send");
			view.addObject("payworks", payVO);
			
		}catch(Exception ex){
			LOGGER.info("["+"] " + gson.toJson(request));
			LOGGER.error("[ERROR AL ENVIAR COBRO 3DS]"+"["+"]", ex);
			view = new ModelAndView("payworks/redirect_response");
			MCTransafersResponse transfersresponse = new MCTransafersResponse();
			transfersresponse.setIdError(90);
			if(request.getIdioma().equals("es")){
				transfersresponse.setMensajeError("Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. "
								+ "Por favor vuelva a intentarlo en unos minutos.");
			}
			else{
				transfersresponse.setMensajeError("Sorry for the inconvenience, we detected an error when authorizing your transaction. "
                 + "Please try again in a few minutes.");
			}
			
			//view.setViewName("transfers_response");
			view.addObject("json", gson.toJson(transfersresponse));
		
		}
		return view;
	}
	
	// respuesta 3ds banorte y envio de confirmacion a banorte
		public ModelAndView payworksRec3DRespuesta(String cadena, ModelMap modelo){
			HashMap<String, Object> resp = new HashMap<String, Object>();
			ModelAndView mav = null;
			try{
				cadena = cadena.replace(" ", "+");
				LOGGER.debug("Cadena procesaRespuesta3DPayworks: " + cadena);
				String[] spl = cadena.split("&");
				String[] data = null;
				for(String cad: spl){
					LOGGER.debug("data:  " + cad);
					data = cad.split("=");
					resp.put(data[0], data.length >= 2? data[1]: "");
				}
				
				if(resp.containsKey("Status") && "200".equals(resp.get("Status"))){
					AfiliacionVO afiliacion = mapper.buscaAfiliacion("BANORTEH2HLCPF");
					LOGGER.debug("idBitacora "+ resp.get("Reference3D").toString());
					 TBitacoraBanorte bitacoraBanorte =   mapper.getBitacoraBanorte(Long.parseLong(resp.get("Reference3D").toString()));
					Card card = mapper.getCard(bitacoraBanorte.getId_usuario(), bitacoraBanorte.getId_card());
					mav=new ModelAndView("payworks/comercioPAYW2");
					mav.addObject("payworks", resp);
					
					resp.put("ID_AFILIACION", afiliacion.getIdAfiliacion());
					resp.put("USUARIO", afiliacion.getUsuario());
					resp.put("CLAVE_USR", afiliacion.getClaveUsuario());
					resp.put("CMD_TRANS", "VENTA");
					resp.put("ID_TERMINAL", afiliacion.getIdTerminal());
					resp.put("Total", Business.formatoMontoPayworks((String)resp.get("Total")));
					resp.put("MODO", "PRD");
					resp.put("NUMERO_CONTROL", resp.get("Reference3D"));
					resp.put("NUMERO_TARJETA", resp.get("Number"));
					resp.put("FECHA_EXP", ((String)resp.get("Expires")).replaceAll("%2F", ""));
					resp.put("CODIGO_SEGURIDAD",UtilsService.getSMS(card.getCt()));// UtilsService.getSMS(card.getCt()); 
					resp.put("MODO_ENTRADA", "MANUAL");
					resp.put("URL_RESPUESTA", afiliacion.getForwardPath().split(" ")[1]);//Constantes.VAR_PAYW_URL_BACK_W2
					resp.put("IDIOMA_RESPUESTA", "ES");
					resp.put("CODIGO_SEGURIDAD", UtilsService.getSMS(card.getCt())); //UtilsService.getSMS(card.getCt()); 
					//resp.put("ESTATUS_3D", "200");
					
					if(resp.get("XID") != null){
						resp.put("XID", ((String)resp.get("XID")).replaceAll("%3D", "="));
						resp.put("CAVV", ((String)resp.get("CAVV")).replaceAll("%3D", "="));
					}
					
				
					
					LOGGER.info("DATOS ENVIADO A PAYWORK2: "+resp.toString());	
				}
				else{
					LOGGER.debug("[PAGO RECHAZADO]");
				
					TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
					bitacora_banorte.setId_bitacora(Long.parseLong((String)resp.get("Reference3D")));
					bitacora_banorte.setTexto( resp.get("Status") != null ?  (String)Constantes.errorPayW.get(resp.get("Status")) : "" );
					bitacora_banorte.setReferencia("");
					bitacora_banorte.setResultado_payw("");
					bitacora_banorte.setResultado_aut("");
					bitacora_banorte.setBanco_emisor("");
					bitacora_banorte.setTipo_tarjeta("");
					bitacora_banorte.setMarca_tarjeta((String)resp.get("CardType"));
					bitacora_banorte.setCodigo_aut("");
					bitacora_banorte.setBank_reference("");
					bitacora_banorte.setTransaction_number(0);
					bitacora_banorte.setStatus3DS( resp.get("Status") != null ? (String)resp.get("Status") : "");
					
					mapper.updateBitacoraBanorteOBJ(bitacora_banorte);
					
					mav = new ModelAndView("payworks/pagina-error");
					//mav.addObject("mensajeError", "El pago fue rechazado. " + 
						//	(resp.get("Status") != null? "Clave: " + resp.get("Status"): "") + ", Descripcion: " + Constantes.errorPayW.get(resp.get("Status")) );
					
					MCTransafersResponse transfersresponse = new MCTransafersResponse();
					transfersresponse.setIdError(91);
						transfersresponse.setMensajeError("El pago fue rechazado. " + 
								(resp.get("Status") != null? "Clave: " + resp.get("Status"): "") + ", Descripcion: " + Constantes.errorPayW.get(resp.get("Status")));
					
					
					//view.setViewName("transfers_response");
						mav.addObject("json", gson.toJson(transfersresponse));
					
				}

				
			}catch(Exception ex){
				LOGGER.error("ERROR AL PROCESAR RESPUESTA 3DS BANORTE "+ cadena, ex);
				LOGGER.error("ERROR ", ex);
				mav = new ModelAndView("payworks/pagina-error");
				MCTransafersResponse transfersresponse = new MCTransafersResponse();
				transfersresponse.setIdError(96);
				/*transfersresponse.setMensajeError("Sorry for the inconvenience, we detected an error when authorizing your transaction. "
	                 + "Please try again in a few minutes.");*/
				
					transfersresponse.setMensajeError("Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. "
									+ "Por favor vuelva a intentarlo en unos minutos.");
				
				mav.addObject("json", gson.toJson(transfersresponse));
				
			}
			
			return mav;
		}
		
		public ModelAndView payworks2RecRespuesta(String NUMERO_CONTROL, String REFERENCIA, String FECHA_RSP_CTE, 
				String TEXTO, String RESULTADO_PAYW, String FECHA_REQ_CTE, String CODIGO_AUT, String CODIGO_PAYW, String RESULTADO_AUT, 
				String BANCO_EMISOR, String ID_AFILIACION, String TIPO_TARJETA, String MARCA_TARJETA, ModelMap modelo, HttpServletRequest request){
			ModelAndView mav = new ModelAndView("payworks/transfers_response");
			MCTransafersResponse responseTransfer = new MCTransafersResponse();
			String msg;
			try{
				LOGGER.debug("RESPUESTA PAYWORKS: NUMERO_CONTROL: " + NUMERO_CONTROL+" REFERENCIA: " + REFERENCIA
						+" FECHA_RSP_CTE: " + FECHA_RSP_CTE+" TEXTO: " + TEXTO+" RESULTADO_AUT: " + RESULTADO_AUT
						+" RESULTADO_PAYW: " + RESULTADO_PAYW+" FECHA_REQ_CTE: " + FECHA_REQ_CTE+" CODIGO_PAYW: " + CODIGO_PAYW
						+" CODIGO_PAYW: " + CODIGO_AUT+" BANCO_EMISOR: " + BANCO_EMISOR+" TIPO_TARJETA: " + TIPO_TARJETA
						+" MARCA_TARJETA: " + MARCA_TARJETA);
				long id_bitacora =  Long.parseLong(NUMERO_CONTROL);
				TBitacoraBanorte bitacoraBanorte = mapper.getBitacoraBanorte(id_bitacora);
				double total = bitacoraBanorte.getComision() + bitacoraBanorte.getAmount();
				
				if (RESULTADO_PAYW != null && "A".equals(RESULTADO_PAYW)) {
				 /// pago aceptado
					User user = mapper.getUserData(bitacoraBanorte.getId_usuario());
					PaymentRequest paymentRequest = new PaymentRequest();
					paymentRequest.setAccountId(bitacoraBanorte.getAccount_id());
					paymentRequest.setAmount(bitacoraBanorte.getAmount()+bitacoraBanorte.getPropina());
					paymentRequest.setConcept("La cuenta por favor"); //TODO
					paymentRequest.setIdCard(bitacoraBanorte.getId_card());
					paymentRequest.setIdioma(bitacoraBanorte.getIdioma());
					paymentRequest.setIdUser(bitacoraBanorte.getId_usuario());
					paymentRequest.setAuthProcom(CODIGO_AUT);
					paymentRequest.setRefProcom(REFERENCIA);
					mav = EnqueuePayment(paymentRequest,id_bitacora);
					responseTransfer = gson.fromJson((String)mav.getModel().get("json"),MCTransafersResponse.class);
					responseTransfer.setComision(bitacoraBanorte.getComision());
					responseTransfer.setTarjeta(AddcelCrypto.encryptHard(UtilsService.getSMS(bitacoraBanorte.getTarjeta_transferencia())));
					responseTransfer.setReferenciaNeg(bitacoraBanorte.getReferencia_negocio());
					responseTransfer.setIdBitacora(bitacoraBanorte.getId_bitacora());
					responseTransfer.setMontoTransfer(bitacoraBanorte.getAmount()+bitacoraBanorte.getPropina()+bitacoraBanorte.getComision());
					mav.addObject("json", gson.toJson(responseTransfer));
					
					//ENVIANDO CORREO DE CONFOIRMACION PAGO
					HashMap< String, String> datos = new HashMap<String, String>();
					datos.put("NOMBRE", user.getUsr_nombre());
					datos.put("FECHA",DFormat.format(new Date()));
					
					datos.put("AUTBAN",CODIGO_AUT);
					datos.put("IMPORTE",bitacoraBanorte.getAmount()+"");
					datos.put("PROPINA",bitacoraBanorte.getPropina()+"");
					datos.put("COMISION", bitacoraBanorte.getComision()+"");
					datos.put("TOTAL", (bitacoraBanorte.getAmount()+bitacoraBanorte.getPropina()+bitacoraBanorte.getComision())+"");
					ProjectMC project = mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");//mapper.getProjectMC("GestionUsuarios", "userMCActivate");
					Business.SendMailMicrosoft(user.geteMail(),mapper.getParameter("@MESSAGE_LCPF_PAGO_ES"), "¡La cuenta por favor!", datos,project.getUrl());
					
					
				}else
				{
					msg = 	("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADA": 
							"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADA":
							"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida")
							+ ": " + TEXTO ;
					LOGGER.debug("[Mensaje Banorte] " + msg);
					
					responseTransfer.setIdError(-99);
					responseTransfer.setMensajeError(msg);
					responseTransfer.setTarjeta(AddcelCrypto.encryptHard(UtilsService.getSMS(bitacoraBanorte.getTarjeta_transferencia())));
					responseTransfer.setComision(bitacoraBanorte.getComision());
					SimpleDateFormat DFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");;
					responseTransfer.setFecha(DFormat.format(new Date()));
					responseTransfer.setMontoTransfer(bitacoraBanorte.getAmount()+bitacoraBanorte.getPropina()+bitacoraBanorte.getComision());
					responseTransfer.setReferenceBanorte("");
					responseTransfer.setReferenciaNeg(bitacoraBanorte.getReferencia_negocio());
					responseTransfer.setIdBitacora(bitacoraBanorte.getId_bitacora());
					mav.addObject("json", gson.toJson(responseTransfer));
					
				}
					
					TBitacoraBanorte bitacora_banorte = new TBitacoraBanorte();
					bitacora_banorte.setId_bitacora(id_bitacora);
					bitacora_banorte.setTexto(TEXTO);
					bitacora_banorte.setReferencia(REFERENCIA);
					bitacora_banorte.setResultado_payw(RESULTADO_PAYW);
					bitacora_banorte.setResultado_aut(RESULTADO_AUT);
					bitacora_banorte.setBanco_emisor(BANCO_EMISOR);
					bitacora_banorte.setTipo_tarjeta(TIPO_TARJETA);
					bitacora_banorte.setMarca_tarjeta(MARCA_TARJETA);
					bitacora_banorte.setCodigo_aut(CODIGO_AUT);
					bitacora_banorte.setBank_reference(responseTransfer.getReferenceBanorte()); //responseTransfer.getReferenceBanorte()
					bitacora_banorte.setTransaction_number(Long.parseLong(responseTransfer.getReferenceBanorte() != "" ? responseTransfer.getReferenceBanorte() : "0")); //Long.parseLong(responseTransfer.getReferenceBanorte())
					bitacora_banorte.setStatus3DS("200");
					
					mapper.updateBitacoraBanorteOBJ(bitacora_banorte);
					
					
				
				
				
				
			}catch(Exception ex){
				LOGGER.error("[ERROR AL PROCESAR RESPUESTA BANORTE payworks2RecRespuesta] " + NUMERO_CONTROL, ex);
				mav = new ModelAndView("payworks/pagina-error");
				MCTransafersResponse transfersresponse = new MCTransafersResponse();
				transfersresponse.setIdError(97);
				transfersresponse.setMensajeError("Sorry for the inconvenience, we detected an error when authorizing your transaction. "
	                 + "Please try again in a few minutes.");
				mav.addObject("json", gson.toJson(transfersresponse));
			}
			return mav;
		}
		
		
		/*
		 * Encola una transacción para transferencia
		 * Se compara RFC,NUM CUENTA, COD BANCO, TIPO CUENTA para determinar que estén registradas
		 */
		public ModelAndView EnqueuePayment(PaymentRequest paymentRequest, long idBitacora){
			EnqueuePaymentRequest payment = new EnqueuePaymentRequest();
			EnqueueTransactionResponse response = new EnqueueTransactionResponse();
			MCTransafersResponse responseTransfer = new MCTransafersResponse();
			ModelAndView view = new ModelAndView("payworks/transfers_response");
			BaseResponse McResponse = new BaseResponse();
			try{
				
				TBitacoraVO bitacora = new TBitacoraVO();
				
				//SimpleDateFormat DFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
				responseTransfer.setAuthProcom(paymentRequest.getAuthProcom());
				responseTransfer.setComision(1);
				responseTransfer.setFecha(DFormat.format(new Date()));
				responseTransfer.setMontoTransfer(paymentRequest.getAmount());
				
				
				
				User user = mapper.getUserData(paymentRequest.getIdUser());
				Card card = mapper.getCard(paymentRequest.getIdUser(),paymentRequest.getIdCard());
				AccountRecord fromAccount = new AccountRecord();
				
				responseTransfer.setTarjeta(Business.getSMS(card.getNumerotarjeta()));
				
				fromAccount.setActNumber("0276948693"); //card.getNum_cuenta()
				fromAccount.setActType("ACT"); //card.getAct_type()
				fromAccount.setBankCode("072"); //card.getIdbanco()
				fromAccount.setContact("Jorge Silva Gallegos"); //card.getNombre_tarjeta()
				fromAccount.setEmail("jorge@mobilecardmx.com"); //user.geteMail()
				fromAccount.setHolderName("ADCEL SA DE CV"); //card.getNombre_tarjeta()
				fromAccount.setPhone("5555403124"); //user.getUsr_telefono()
				fromAccount.setRfc("ADC090715DW3");
				
				AccountRecord toAccount = mapper.getAccount(paymentRequest.getAccountId());
				//	LOGGER.debug("To Account: " + paymentRequest.getAccountId() + " " + gson.toJson(toAccount));
				//LOGGER.debug("From Account: " + fromAccount);
				
				
				
				payment.setAmount(paymentRequest.getAmount());
				payment.setClientReference(idBitacora+""); //"ACC000011"
				payment.setDescription(paymentRequest.getConcept());
				payment.setFromAccount(fromAccount);
				payment.setToAccount(toAccount);
				//payment.setToken(login.getToken());
				
				//response = h2hclient.EnqueuePayment(payment,paymentRequest.getIdioma());
				//POR EL MOMENTO SE COMENTO LA LINEA DE ARRIBA PARA NO REALIZAR LA TRANSFERENCIA EN ESE MOMENTO
				response.setClientReference(idBitacora+"");
				response.setTransactionReference("0");
				ServiceResult result = new ServiceResult();
				result.setMessage("Pago Exitoso");
				result.setSuccess(true);
				response.setResult(result);
				 /*{"montoTransfer":2.0,"idBitacora":0,"comision":1.0,"authProcom":"813233","referenceBanorte":"104",
					 "refProcom":"317562266537","tarjeta":"0276948693","fecha":"02/07/2018 12:00:11 AM","idError":0,
					 "mensajeError":"Transaction enqueued for later processing."}*/

				
				
				
			//	LOGGER.debug("[RESPUESTA PAGO] " + gson.toJson(response));
				
				if(response.getResult().isSuccess()){
					//String responseBD = mapper.H2HBanorte_Business(paymentRequest.getIdUser(), bitacora.getIdBitacora(), Long.parseLong(response.getTransactionReference()), "", 1, "es");
					LOGGER.debug("[Transaccion guardada] Exito");
					mapper.updateTbitacora(idBitacora,1,paymentRequest.getAuthProcom());
					/*HashMap< String, String> datos = new HashMap<String, String>();
					datos.put("NOMBRE", toAccount.getContact());
					datos.put("FECHA",DFormat.format(new Date()));
					
					datos.put("IMPORTE", payment.getAmount()+"");
					datos.put("CUENTA", toAccount.getActNumber());*/
					
					//ENVIANDO CORREO DE CONFOIRMACION PAGO
					HashMap< String, String> datos = new HashMap<String, String>();
					datos.put("NOMBRE", toAccount.getHolderName().trim());
					datos.put("FECHA",DFormat.format(new Date()));
					datos.put("IMPORTE",payment.getAmount()+"");
					ProjectMC project = mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");
	    			 
					try{
						Business.SendMailMicrosoft(toAccount.getEmail(),mapper.getParameter("@MESSAGE_LCPF_COMERCIO_NOTIFICACION"), "¡La cuenta por favor!", datos, project.getUrl());
					}catch(Exception ex){
						LOGGER.error("[error al enviar correo de notificacion ] ", ex);
					}
					
				}
				else
				{
					LOGGER.debug("[Transaccion guardada] error al cobrar");
					mapper.updateTbitacora(idBitacora,2,"");
				}
				
				SimpleDateFormat DFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
				responseTransfer.setAuthProcom(paymentRequest.getAuthProcom());
				responseTransfer.setComision(1);
				responseTransfer.setFecha(DFormat.format(new Date()));
				responseTransfer.setIdError(response.getResult().isSuccess() ? 0 : 98);
				responseTransfer.setMensajeError(response.getResult().getMessage() == null ? "" : response.getResult().getMessage());
				responseTransfer.setMontoTransfer(paymentRequest.getAmount());
				responseTransfer.setReferenceBanorte(response.getTransactionReference());
				responseTransfer.setTarjeta(fromAccount.getActNumber());
				
				responseTransfer.setIdError(response.getResult().isSuccess() ? 0 : 98);
				responseTransfer.setMensajeError(response.getResult().getMessage() == null ? "" : response.getResult().getMessage());
				responseTransfer.setReferenceBanorte(response.getTransactionReference());
				responseTransfer.setRefProcom(paymentRequest.getRefProcom());
				
				McResponse.setIdError(response.getResult().isSuccess() ? 0 : 98);
				McResponse.setMensajeError(response.getResult().getMessage() == null ? "" : response.getResult().getMessage());
				LOGGER.debug("JSON: " +  gson.toJson(responseTransfer));
				view.addObject("json", gson.toJson(responseTransfer));
				return view;//McResponse;
			}catch(Exception ex){
				//LOGGER.error("[ERROR AL ENCOLAR TRANSFERENCIA]  " +  gson.toJson(paymentRequest));
				LOGGER.error("[ERROR AL ENCOLAR PAGO] " , ex);
				responseTransfer.setIdError(98);
				responseTransfer.setMensajeError("Error al procesar Transaferencia");
				view.addObject("json", gson.toJson(responseTransfer));
				return view;//McResponse;
			}
		}
		

}
