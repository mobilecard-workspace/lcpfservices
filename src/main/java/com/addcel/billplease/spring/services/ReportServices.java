package com.addcel.billplease.spring.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.billplease.mybatis.model.mapper.ServiceMapper;
import com.addcel.billplease.mybatis.model.vo.Establecimiento;
import com.addcel.billplease.mybatis.model.vo.FromAccount;

@Service
public class ReportServices {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReportServices.class);
	 
	@Autowired
	private ServiceMapper mapper;
	
	public List<FromAccount> getEstablecimientos(){
		return mapper.getEstablecimientos();
	}
}
