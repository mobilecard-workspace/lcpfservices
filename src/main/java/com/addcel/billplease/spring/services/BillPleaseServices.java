package com.addcel.billplease.spring.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.axis.encoding.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.billplease.mybatis.model.mapper.ServiceMapper;
import com.addcel.billplease.mybatis.model.vo.Bank;
import com.addcel.billplease.mybatis.model.vo.Establecimiento;
import com.addcel.billplease.mybatis.model.vo.Operador;
import com.addcel.billplease.mybatis.model.vo.ProjectMC;
import com.addcel.billplease.mybatis.model.vo.TBitacoraBanorte;
import com.addcel.billplease.spring.model.request.Login;
import com.addcel.billplease.spring.model.request.LoginAdmin;
import com.addcel.billplease.spring.model.request.LoginOper;
import com.addcel.billplease.spring.model.request.getEstRequest;
import com.addcel.billplease.spring.model.response.AddOpResponse;
import com.addcel.billplease.spring.model.response.BankCodesResponse;
import com.addcel.billplease.spring.model.response.BaseResponse;
import com.addcel.billplease.spring.model.response.CuentasEstResponse;
import com.addcel.billplease.spring.model.response.LoginResponse;
import com.addcel.billplease.spring.model.response.MCSignUpResponse;
import com.addcel.billplease.spring.model.response.MCTransafersResponse;
import com.addcel.billplease.spring.model.response.VerifyResponse;
import com.addcel.billplease.utils.Constantes;
import com.addcel.billplease.utils.UtilsMC;
import com.addcel.billplease.spring.services.H2HCLIENT;
import com.addcel.billplease.client.model.EnqueueAccountSignUpRequest;
import com.addcel.billplease.client.model.AccountRecord;
import com.addcel.billplease.utils.Business;
import com.addcel.billplease.client.model.EnqueueTransactionResponse;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;


 @Service
public class BillPleaseServices {

	 private static final Logger LOGGER = LoggerFactory.getLogger(BillPleaseServices.class);
	 private Gson gson = new Gson();
	 private H2HCLIENT h2hclient = new H2HCLIENT();
	 
	 @Autowired
	 ServiceMapper mapper;
	 
	
	 public LoginResponse OperLogin(LoginOper login){
		   LoginResponse response = new LoginResponse();
		 try{
			 Operador operador = mapper.loginOperadorByEstablecimiento(login.getUsuario(),login.getPass(), login.getId_establecimiento());
			 if(operador != null){
				 Establecimiento est = mapper.getEstablecimiento(operador.getId_establecimiento());
				 response.setId(operador.getId());
				 response.setStatus(operador.getEstatus());
				 response.setDireccion_establecimiento(est.getDireccion_establecimiento());
				 response.setNombre_establecimiento(est.getNombre_establecimiento());
				 response.setId_establecimiento(operador.getId_establecimiento());
				 response.setIdError(0);
				 response.setMensajeError("");
			 }else{
				 response.setId(0);
				 response.setStatus(0);
				 response.setIdError(1);
				 response.setDireccion_establecimiento("");
				 response.setNombre_establecimiento("");
				 response.setId_establecimiento(0);
				 if(login.getIdioma().equalsIgnoreCase("es"))
					 response.setMensajeError("Introduzca usuario y contraseña válidos");
				 else
					 response.setMensajeError("Enter valid username and password");
			 }
			 
		 }catch(Exception ex){
			 LOGGER.error("[ERROR EN LOGIN ]"+ login.getUsuario(), ex);
			 response.setId(0);
			 response.setStatus(0);
			 response.setIdError(90);
			 if(login.getIdioma().equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, vuelva a intentarlo");
			 else
				 response.setMensajeError("Unexpected error, try again");
		 }
		 return response;
	 }
	 
	  public LoginResponse OperLogin(Login login){
		   LoginResponse response = new LoginResponse();
		 try{
			 Operador operador = mapper.loginOperador(login.getUsuario(), login.getPass());
			 if(operador != null){
				 Establecimiento est = mapper.getEstablecimiento(operador.getId_establecimiento());
				 response.setId(operador.getId());
				 response.setStatus(operador.getEstatus());
				 response.setDireccion_establecimiento(est.getDireccion_establecimiento());
				 response.setNombre_establecimiento(est.getNombre_establecimiento());
				 response.setId_establecimiento(operador.getId_establecimiento());
				 response.setIdError(0);
				 response.setMensajeError("");
			 }else{
				 response.setId(0);
				 response.setStatus(0);
				 response.setIdError(1);
				 response.setDireccion_establecimiento("");
				 response.setNombre_establecimiento("");
				 response.setId_establecimiento(0);
				 if(login.getIdioma().equalsIgnoreCase("es"))
					 response.setMensajeError("Introduzca usuario y contraseña válidos");
				 else
					 response.setMensajeError("Enter valid username and password");
			 }
			 
		 }catch(Exception ex){
			 LOGGER.error("[ERROR EN LOGIN ]"+ login.getUsuario(), ex);
			 response.setId(0);
			 response.setStatus(0);
			 response.setIdError(99);
			 if(login.getIdioma().equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, vuelva a intentarlo");
			 else
				 response.setMensajeError("Unexpected error, try again");
		 }
		 return response;
	 }
	 
	 public LoginResponse EstabLogin(LoginAdmin login){
		 LoginResponse response = new LoginResponse();
		 try{
			 Establecimiento establecimiento = mapper.loginEstablecimientoById(login.getId(), login.getPass());
			 if(establecimiento != null){
				 response.setId(establecimiento.getId());
				 response.setStatus(establecimiento.getEstatus());
				 response.setDireccion_establecimiento(establecimiento.getDireccion_establecimiento());
				 response.setNombre_establecimiento(establecimiento.getNombre_establecimiento());
				 response.setId_establecimiento(establecimiento.getId());
				 response.setIdError(0);
				 response.setMensajeError("");
			 }else{
				 response.setId(0);
				 response.setStatus(0);
				 response.setDireccion_establecimiento("");
				 response.setNombre_establecimiento("");
				 response.setId_establecimiento(0);
				 response.setIdError(1);
				 if(login.getIdioma().equalsIgnoreCase("es"))
					 response.setMensajeError("La contraseña es incorrecta");
				 else
					 response.setMensajeError("Incorrect password");
			 }
		 }catch(Exception ex){
			 LOGGER.error("[ERRO LOGIN ESTABLECIMIENTO] " + login.getId(), ex);
			 response.setId(0);
			 response.setStatus(0);
			 response.setIdError(90);
			 if(login.getIdioma().equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, vuelva a intentarlo");
			 else
				 response.setMensajeError("Unexpected error, try again");
		 }
		 return response;
	 }
	 public LoginResponse EstabLogin(Login login){
		 LoginResponse response = new LoginResponse();
		 try{
			 Establecimiento establecimiento = mapper.loginEstablecimiento(login.getUsuario(), login.getPass());
			 if(establecimiento != null){
				 response.setId(establecimiento.getId());
				 response.setStatus(establecimiento.getEstatus());
				 response.setDireccion_establecimiento(establecimiento.getDireccion_establecimiento());
				 response.setNombre_establecimiento(establecimiento.getNombre_establecimiento());
				 response.setId_establecimiento(establecimiento.getId());
				 response.setIdError(0);
				 response.setMensajeError("");
			 }else{
				 response.setId(0);
				 response.setStatus(0);
				 response.setDireccion_establecimiento("");
				 response.setNombre_establecimiento("");
				 response.setId_establecimiento(0);
				 response.setIdError(1);
				 if(login.getIdioma().equalsIgnoreCase("es"))
					 response.setMensajeError("Introduzca usuario y contraseña válidos");
				 else
					 response.setMensajeError("Enter valid username and password");
			 }
		 }catch(Exception ex){
			 LOGGER.error("[ERRO LOGIN ESTABLECIMIENTO] " + login.getUsuario(), ex);
			 response.setId(0);
			 response.setStatus(0);
			 response.setIdError(99);
			 if(login.getIdioma().equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, vuelva a intentarlo");
			 else
				 response.setMensajeError("Unexpected error, try again");
		 }
		 return response;
	 }
	 
	 public BaseResponse AltaEstablecimiento(Establecimiento est,String idioma){
		 BaseResponse response = new BaseResponse();
		 try{
			 LOGGER.debug("[ALTA ESTABLECIMIENTO]" + est.getNombre_establecimiento());
			 est.setEstatus(99);
			 
			 int cont_user = mapper.searchEstab(est.getUsuario());
			
			 if(cont_user == 0){
			 
				 Bank bank = mapper.getBanco(est.getId_banco());
				 //insertar cuenta en banorte
				 EnqueueAccountSignUpRequest signup = new EnqueueAccountSignUpRequest();
				 long clientReference = mapper.getMaxIdBitacora();
				 String act_type = est.getCuenta_clabe().length() == 10 || est.getCuenta_clabe().length() == 11 ? "ACT" : "CLABE"; 
				 
				//verificar clave banco con cuenta
					if( act_type.equals("CLABE")){
						if(!est.getCuenta_clabe().substring(0, 3).equals(bank.getClave())){
							response.setIdError(92);
							if(idioma.equals("es"))
								response.setMensajeError("Verifique que la cuenta introducida pertenezca al banco seleccionado.");
							else
								response.setMensajeError("Verify that the account entered belongs to the selected bank.");
							LOGGER.info("[DETECTAMOS CUENTA INCONSISTENTE CODIGOBANK vs NUMCUENTA] " );
							return response;
						}
					}
				 
					AccountRecord account = new AccountRecord();
					account.setActNumber(est.getCuenta_clabe().trim());
					account.setActType(act_type);
					account.setBankCode(bank.getClave());
					account.setContact(est.getRepresentante_legal()== null ? "" : Business.removeAcentos(est.getRepresentante_legal()).trim() );//opcional
					account.setEmail(est.getEmail_contacto().trim());
					account.setHolderName(Business.removeAcentos(est.getNombre_establecimiento()).trim()); //obligatorio
					account.setPhone(est.getTelefono_contacto()== null ? "" :est.getTelefono_contacto().trim() ); //opcional
					account.setRfc(est.getRfc().trim());
					signup.setAccount(account);
					signup.setClientReference(new Date().getSeconds()+""+clientReference); //verificar al final
					signup.setIdAccount("");
					EnqueueTransactionResponse responseBanorte = h2hclient.EnqueueAccountSignUp(signup, idioma);
				 if(responseBanorte.getResult().isSuccess()){
					 long id_trans = mapper.getAccountId(Long.parseLong(responseBanorte.getTransactionReference()));
					 est.setId_account(id_trans);
				 }
				 else
					 est.setId_account(0);
				 
				 est.setComision_fija(0); //bank.getComision_fija()
				 est.setComision_porcentaje(bank.getComision_porcentaje());
				 est.setUrlLogo(mapper.getProjectMC("LCPFServices", "getDefaultLogo").getUrl());
				 //est.setUrlLogo("https://mobilecard.mx:8444/LCPFServices/logo_default.png");//TODO
				 //QA http://199.231.160.203:8089
				 // PROD https://mobilecard.mx:8444/LCPFServices/logo_default.png
				 mapper.insertEstablecimiento(est);
				 
				 
				 //ENVIAR CORREO DE CONFIRMACION
				 LOGGER.debug("id establecimiento: " + est.getId());
				 String value =  AddcelCrypto.encryptHard(est.getId()+":"+est.getId());
				 LOGGER.debug("base 64: "+ Base64.encode(value.getBytes()));
				 String param = "?codigo="+Base64.encode(value.getBytes());
				 ProjectMC projects = mapper.getProjectMC("LCPFServices", "adminActivate");
				 String url =  projects.getUrl().replace("{idioma}", idioma)+param;//Constantes.ACTIVATION_URL.replace("{idioma}", idioma)+param;
				 LOGGER.debug("[ACTIVATE URL] " + url);
				 
				 if(idioma.equalsIgnoreCase("es"))
	 			    UtilsMC.emailSend(est.getEmail_contacto(), mapper.getParameter("@MENSAJE_ACTIVATE_LCPF_ES"), mapper.getParameter("@ASUNTO_ACTIVATE_LCPF_ES"), est.getEmail_contacto(), url, "", est.getRepresentante_legal(),new String[]{Constantes.Path_images+"user_registry.PNG"});
	 			 else
	 				 UtilsMC.emailSend(est.getEmail_contacto(), mapper.getParameter("@MENSAJE_ACTIVATE_LCPF_EN"), mapper.getParameter("@ASUNTO_ACTIVATE_LCPF_EN"), est.getEmail_contacto(), url,"",  est.getRepresentante_legal(),new String[]{Constantes.Path_images+"user_registry.PNG"}); 
				 
				 response.setIdError(0);
				 if(idioma.equalsIgnoreCase("es"))
					 response.setMensajeError("Se ha registrado con éxito, se envió un correo electrónico para confirmar registro.");
				 else
					 response.setMensajeError("Registered successfully, email was sent to confirm.");
			 }else{
				 
				 response.setIdError(98);
				 if(idioma.equalsIgnoreCase("es"))
					 response.setMensajeError("Existe un usuario con el email introducido");
				 else
					 response.setMensajeError("There is already a user with the email entered.");
			 }
			
		 }catch(Exception ex){
			 LOGGER.error("[ERRO LOGIN ESTABLECIMIENTO] " + est.getNombre_establecimiento(),ex);
			 response.setIdError(99);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, vuelva a intentarlo");
			 else
				 response.setMensajeError("Unexpected error, try again");
		 }
		 return response;
	 }
	 
	 public BaseResponse AdminUpdate(Establecimiento establecimiento, String idioma){
		 BaseResponse response = new BaseResponse();
		 try{
			 
			 LOGGER.debug("[INICIANDO ACTUALIZACIÓN ESTABLECIMIENTO]" + establecimiento.getId());
			 mapper.updateEstab(establecimiento);
			 response.setIdError(0);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Datos Actualizados");
			 else
				 response.setMensajeError("Updated data");
			 
		 }catch(Exception ex){
			 LOGGER.error("[ERROR UPDATE ESTABLECIMIENTO]"+ establecimiento.getId(), ex);
			 response.setIdError(99);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, vuelva a intentarlo");
			 else
				 response.setMensajeError("Unexpected error, try again");
		 }
		 return response;
	 }
	 
	 public BaseResponse adminPassUpdate(String idioma, long idAdmin, String newPass){
		 
		 BaseResponse response = new BaseResponse();
		 try{
			 LOGGER.debug("[ACTUALIZANDO PASS]");
			 Establecimiento est = mapper.getEstablecimiento(idAdmin);
			 //String pass = AddcelCrypto.decryptHard(newPass);
			// newPass = Business.encode(pass);
			 mapper.AdminPassUpdate(idAdmin, newPass,1);
			 response.setIdError(0);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Contraseña actualizada");
			 else
				 response.setMensajeError("Updated password");
			 HashMap< String, String> datos = new HashMap<String, String>();
			 datos.put("@USUARIO", est.getUsuario());
			 datos.put("@PASSWORD", "");
			 ProjectMC project = mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");
			 
			 Business.SendMailMicrosoft(est.getEmail_contacto(), mapper.getParameter("@MESSAGE_UPDATE_EMAIL_LCPF_ADMIN_ES"), "La cuenta por favor", datos,project.getUrl());
		 }catch(Exception ex){
			 LOGGER.error("[ERROR AL ACTUALIZAR PASSWORD]" + idAdmin, ex);
			 response.setIdError(98);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, intente mas tarde");
			 else
				 response.setMensajeError("Unexpected error, try later");
		 }
		 return response;
	 }
	 
	 public BaseResponse updateOperadorPass(String idioma, long idAdmin, long idOper, String newPass){
		 BaseResponse response = new BaseResponse();
		 try{
			 
			 LOGGER.debug("[ACTUALIZANDO PASS OPERADOR]");
			 Operador oper =  mapper.getOperador(idOper);
			 Establecimiento est = mapper.getEstablecimiento(idAdmin);
			 //String pass = AddcelCrypto.decryptHard(newPass);
			 //newPass = Business.encode(pass);
			 if(oper.getId_establecimiento() == idAdmin ){
				mapper.AdminPassOperadorUpdate(idAdmin, idOper, newPass,1);
				response.setIdError(0);
				if(idioma.equalsIgnoreCase("es"))
					response.setMensajeError("Contraseña actualizada");
				else
					response.setMensajeError("Updated password");
				
			 }else{
				 response.setIdError(97);
				 if(idioma.equalsIgnoreCase("es"))
					 response.setMensajeError("El operador no pertenece a su dominio");
				 else
					 response.setMensajeError("The operator does not belong to your domain");
				 HashMap< String, String> datos = new HashMap<String, String>();
				 datos.put("@USUARIO", est.getUsuario());
				 datos.put("@PASSWORD", "");
				 ProjectMC project = mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");
				 
				 Business.SendMailMicrosoft(est.getEmail_contacto(), mapper.getParameter("@MESSAGE_UPDATE_EMAIL_LCPF_ADMIN_ES"), "La cuenta por favor", datos,project.getUrl());
			 }
		 }catch(Exception ex){
			 LOGGER.error("[ERROR AL ACTUALIZAR PASS OPERADOR]", ex);
			 response.setIdError(98);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, intente mas tarde");
			 else
				 response.setMensajeError("Unexpected error, try later");
			 
		 }
		 return response;
	 }
	 
	 public BaseResponse AdminResetPass(String idioma, String admin){
		 BaseResponse response = new BaseResponse();
		 try{
			 LOGGER.debug("[RESET PASS]");
			 
			 Establecimiento est = mapper.getEstablecimientoByUser(admin);
			 long idAdmin = est.getId();
			 String newpass = UtilsMC.generatePassword(8);
			 String passEncoder = Business.encode(newpass);
			 mapper.AdminPassUpdate(idAdmin, passEncoder,98);
			 response.setIdError(0);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Se ha enviado a su correo una contraseña provisional");
			 else
				 response.setMensajeError("A temporary password has been sent to your email");
			 HashMap< String, String> datos = new HashMap<String, String>();
			 datos.put("@USUARIO", est.getUsuario());
			 datos.put("@PASSWORD", newpass);
			 ProjectMC project = mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");
			 
			 Business.SendMailMicrosoft(est.getEmail_contacto(), mapper.getParameter("@MESSAGE_RESET_EMAIL_LCPF_ADMIN_ES"), "La cuenta por favor", datos, project.getUrl());
		 }catch(Exception ex){
			 LOGGER.error("[ERROR RESET PASS]", ex);
			 response.setIdError(98);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, intente mas tarde");
			 else
				 response.setMensajeError("Unexpected error, try later");
		 }
		 return response;
	 }
	 
	 public BaseResponse AdminResetPassOperador(String idioma, long idAdmin, long idOperador){
		BaseResponse response = new  BaseResponse();
		try{
			LOGGER.debug("[RESET PASS OPERADOR]");
			 String newpass = UtilsMC.generatePassword(8);
			 String passEncoder = Business.encode(newpass);
			 Establecimiento est = mapper.getEstablecimiento(idAdmin);
			 Operador oper = mapper.getOperador(idOperador);
			 if(est.getId() == oper.getId_establecimiento()){
				 mapper.AdminPassOperadorUpdate(idAdmin, idOperador, passEncoder, 98);
				 response.setIdError(0);
				 if(idioma.equalsIgnoreCase("es"))
					 response.setMensajeError("Se ha enviado a su correo una contraseña provisional");
				 else
					 response.setMensajeError("A temporary password has been sent to your email");
				 HashMap< String, String> datos = new HashMap<String, String>();
				 datos.put("@USUARIO", est.getUsuario());
				 datos.put("@PASSWORD", newpass);
				 ProjectMC project = mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");
				 
				 Business.SendMailMicrosoft(est.getEmail_contacto(), mapper.getParameter("@MESSAGE_RESET_EMAIL_LCPF_ADMIN_ES"), "La cuenta por favor", datos, project.getUrl());
			 }
			 else{
				 response.setIdError(97);
				 if(idioma.equalsIgnoreCase("es"))
					 response.setMensajeError("El operador no pertenece a su dominio");
				 else
					 response.setMensajeError("The operator does not belong to your domain");
			 }
		}catch(Exception ex){
			LOGGER.error("[ERROR EN RESET PASS OPERADOR]", ex);
			 response.setIdError(98);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, intente mas tarde");
			 else
				 response.setMensajeError("Unexpected error, try later");
			
		}
		return response;
	 }
	 
	 public BaseResponse AdminDelete(long id, String idioma){
		 BaseResponse response = new BaseResponse();
		 try{
			 LOGGER.debug("[INICIANDO PROCESO DELETE ESTABLECIMIENTO]" + id);
			 mapper.estDelete(id);
			 response.setIdError(0);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Registro eliminado");
			 else
				 response.setMensajeError("Updated record");
		 }catch(Exception ex){
			 LOGGER.error("[ERROR AL ELIMINAR ESTABLECIMIENTO]" + id, ex);
			 response.setIdError(99);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, vuelva a intentarlo");
			 else
				 response.setMensajeError("Unexpected error, try again");
		 }
		 return response;
	 }
	 
	 
	 public AddOpResponse AltaOperador(Operador operador,String idioma){
		 AddOpResponse response = new AddOpResponse();
		 try{
			 LOGGER.debug("[ALTA OPERADOR]"+ operador.getNombre());
			 int user_cont = mapper.searchOper(operador.getUsuario(),operador.getId_establecimiento());
			 if(user_cont == 0){
					 operador.setEstatus(1);
					 mapper.insertOperador(operador);
					 response.setOperadores(mapper.getOperadores(operador.getId_establecimiento()));
					 response.setIdError(0);
					 if(idioma.equalsIgnoreCase("es"))
						 response.setMensajeError("Registro exitoso");
					 else
						 response.setMensajeError("Registered successfully");
			 }else {
				 response.setIdError(98);
				 if(idioma.equalsIgnoreCase("es"))
					 response.setMensajeError("El nombre de usuario no está disponible, introduzca un usuario diferente");
				 else
					 response.setMensajeError("The username is not available, enter a different user");
			 }
		 }catch(Exception ex){
			 LOGGER.error("[ERROR ALTA OPERADOR]" + operador.getNombre(), ex);
			 response.setIdError(91);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, vuelva a intentarlo");
			 else
				 response.setMensajeError("Unexpected error, try again");
		 }
		 return response;
	 }
	 
	 public AddOpResponse updateOperador(Operador operador, String idioma){
		 AddOpResponse response = new AddOpResponse();
		 try{
			 LOGGER.debug("[INICIANDO ACTUALIZACION OPERADOR]");
			 mapper.updateOper(operador);
			 response.setOperadores(mapper.getOperadores(operador.getId()));
			 response.setIdError(0);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Datos Actualizados");
			 else
				 response.setMensajeError("Updated data");
		 }catch(Exception ex){
			 LOGGER.error("[ERROR AL ACTUALIZAR OPERADOR]" + operador.getId(), ex);
			 response.setIdError(99);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, vuelva a intentarlo");
			 else
				 response.setMensajeError("Unexpected error, try again");
		 }
		 return response;
	 }
	 
	 public AddOpResponse OperDelete(long id_est, long id_oper, String idioma){
		 AddOpResponse response = new AddOpResponse();
		 try{
			 LOGGER.debug("[INICIANDO PROCESO DELETE OPERADOR]" + id_oper);
			 mapper.operDelete(id_oper, id_est);
			 response.setOperadores(mapper.getOperadores(id_est));
			 response.setIdError(0);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Registro eliminado");
			 else
				 response.setMensajeError("Record removed");
		 }catch(Exception ex){
			 LOGGER.error("[ERROR AL ELIMINAR OPERADOR]" + id_est, ex);
			 response.setIdError(99);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, vuelva a intentarlo");
			 else
				 response.setMensajeError("Unexpected error, try again");
		 }
		 return response;
	 }
	 /*public AddOpResponse OperDelete(long id_est, long id_oper, String idioma){
		 AddOpResponse response = new AddOpResponse();
		 try{
			 LOGGER.debug("[INICIANDO PROCESO DELETE OPERADOR]" + id_oper);
			 mapper.operDelete(id_oper, id_est);
			 response.setOperadores(mapper.getOperadores(id_est));
			 response.setIdError(0);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Registro eliminado");
			 else
				 response.setMensajeError("Updated record");
		 }catch(Exception ex){
			 LOGGER.error("[ERROR AL ELIMINAR OPERADOR]" + id_est, ex);
			 response.setIdError(99);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, vuelva a intentarlo");
			 else
				 response.setMensajeError("Unexpected error, try again");
		 }
		 return response;
	 }*/
	 
	 public AddOpResponse getOperadores(long id_establecimiento,String idioma){
		 AddOpResponse response = new AddOpResponse();
		 try{
			 LOGGER.debug("[GET OPERADORES] " + id_establecimiento );
			 response.setOperadores(mapper.getOperadores(id_establecimiento));
			 response.setIdError(0);
			 response.setMensajeError("");
		 }catch(Exception ex){
			 LOGGER.error("[ERROR GET OPERADORES]" + id_establecimiento,ex);
			 response.setIdError(99);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, vuelva a intentarlo");
			 else
				 response.setMensajeError("Unexpected error, try again");
		 }
		 return response;
	 }
	 
	 public BankCodesResponse getBankCodes(){
			BankCodesResponse response = new BankCodesResponse();
			try{
				LOGGER.debug("[INICIANDO OBTENCION DE CATALOGO DE BANCOS]");
				response.setBanks( mapper.getBankcodes());
				response.setIdError(0);
				response.setMensajeError("");
			}catch(Exception ex){
				LOGGER.error("[ERROR AL OBTENER BNANCOS]", ex);
				response.setIdError(99);
				response.setMensajeError("Error inesperado, vuelva a intentarlo");
			}
			return response;
		}
	 
	 public ModelAndView userActivate(String codigo, String idioma){
	    	ModelAndView view = new ModelAndView();
	    	LOGGER.debug("[Activando usuario ] " + codigo);
	    	try{
	    		String dataHard = new String(Base64.decode(codigo));
	    		//AddcelCrypto.decryptHard(dataHard);
	    		String[] data = new String(AddcelCrypto.decryptHard(dataHard)).split(":");
	    		String id = data[0];//AddcelCrypto.decryptHard(data[0]);
	    		String pass = data[1];//AddcelCrypto.decryptHard(data[1]);
	    		//LOGGER.debug("user = " + id);
	    		//LOGGER.debug("pass = " + pass);
	    		//mapper.userActivate(user, pass);
	    		
	    		 mapper.userActivate(Long.parseLong(id));
	    		Establecimiento est = mapper.getEstablecimiento(Long.parseLong(id));
	    		view.setViewName("activate");
	    		view.addObject("establecimiento",est.getNombre_establecimiento());
	    		if(idioma.equalsIgnoreCase("es"))
	    			view.addObject("message", "SU CUENTA HA SIDO ACTIVADA");
	    		else
	    			view.addObject("message", "YOUR ACCOUNT HAS BEEN ACTIVATED");

	    		return view;
	    	}catch(Exception ex){
	    		LOGGER.error("[Error al activar usuario] ", ex);
	    		view.setViewName("activate_error");
	    		view.addObject("message", "Error al activar cuenta");
	    		return view;
	    	}
	    }
	 
	 public VerifyResponse UserVerify(long id_usuario, int type,  String idioma){
		 VerifyResponse response = new VerifyResponse();
		 int status = -2;
		 LOGGER.debug("[INICIANDO PROCESO VERIFICAR ESATUS]" + id_usuario);
		 try{
			 if(type==1){
				 //establecimiento
				status = mapper.verifyEsta(id_usuario); 
			 }
			 else 
				 if(type==2){
					 //operador
					 status = mapper.verifyOper(id_usuario);
				 }
			 if(status != -2){
				 response.setStatus(status);
				 response.setIdError(0);
				 if(status == 99){
					 if(idioma.equalsIgnoreCase("es"))
						 response.setMensajeError("Vaya a la bandeja de entrada de su correo y verifique su cuenta mediante el enlace enviado.");
					 else
						 response.setMensajeError("Go to your email account to activate your account.");
				 }else
				     response.setMensajeError("");
			 }else{
				 response.setStatus(0);
				 response.setIdError(98);
				 response.setMensajeError("Error al consultar estatus");
				 
			 }
			 
			 
		 }catch(Exception ex){
			 LOGGER.error("[ERROR AL CONSULTAR ESTATUS USUARIO]" + id_usuario, ex);
			 response.setIdError(97);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, vuelva a intentarlo");
			 else
				 response.setMensajeError("Unexpected error, try again");
		 }
		 
		 return response;
		 
	 }
	 
	 public MCSignUpResponse getFullestablecimientos(String idioma , getEstRequest request){
		 MCSignUpResponse response = new MCSignUpResponse();
		 try{
			 LOGGER.debug("[INICIANDO PROCESO FULL ESTABLECIMIENTOS]");
			 response.setAccounts(mapper.getEstablecimientos());
			 response.setIdError(0);
			 response.setMensajeError("");
		 }catch(Exception ex){
			 LOGGER.error("[ERROR OBTENIENDO FULL ESTABLECIMIENTOS]", ex);
			 response.setIdError(96);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, vuelva a intentarlo");
			 else
				 response.setMensajeError("Unexpected error, try again");
		 }
		 return response;
	 }
	
	 public CuentasEstResponse getCuentasEstablecimiento(String idioma, long id){
		 CuentasEstResponse response = new CuentasEstResponse();
		 try{
			 LOGGER.debug("[INICIANDO PROCESO GET CUENTAS/ESTABLECIMIENTO] " + id);
			 response.setCuentas(mapper.getCuentasEstablecimnientos(id));
			 response.setIdError(0);
			 response.setMensajeError("");
			 
		 }catch(Exception ex){
			 LOGGER.error("[ERROR AL OBTENER CUENTAS ESTABLECIMINTO]" + id, ex);
			 response.setIdError(96);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, vuelva a intentarlo mas tarde.");
			 else
				 response.setMensajeError("Unexpected error, try again a few minutes");
		 }

		 return response;
	 }
	 
	 public MCTransafersResponse setOperador(long idoperador, long idBitacora, String idioma){
		 ModelAndView mav = new ModelAndView("payworks/transfers_response");
			MCTransafersResponse responseTransfer = new MCTransafersResponse();
			SimpleDateFormat DFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
			try{
				LOGGER.debug("[INICIANDO PROCESO SET OPERADOR]" + idoperador+"-"+idBitacora);
				mapper.setOperador(idoperador, idBitacora);
				TBitacoraBanorte bitacora = mapper.getBitacoraBanorte(idBitacora);
				responseTransfer.setAuthProcom(bitacora.getCodigo_aut());
				responseTransfer.setComision(bitacora.getComision());
				responseTransfer.setFecha(DFormat.format(bitacora.getFecha()));
				responseTransfer.setIdBitacora(idBitacora);
				responseTransfer.setIdError(0);
				responseTransfer.setMensajeError("");
				responseTransfer.setMontoTransfer(bitacora.getAmount()+bitacora.getComision() + bitacora.getPropina());
				responseTransfer.setReferenceBanorte(bitacora.getBank_reference());
				responseTransfer.setReferenciaNeg(bitacora.getReferencia_negocio());
				responseTransfer.setRefProcom(bitacora.getReferencia());
				responseTransfer.setTarjeta(AddcelCrypto.encryptHard(UtilsService.getSMS(bitacora.getTarjeta_transferencia())));
				
			}catch(Exception ex){
				LOGGER.error("[ERROR AL ACTUALIZAR CUENTA/OPERADOR]"+ idoperador+"-"+idBitacora, ex );
				responseTransfer.setIdError(80);
				if(idioma.equalsIgnoreCase("es"))
					responseTransfer.setMensajeError("Error inesperado, vuelva a intentarlo mas tarde.");
				else
					responseTransfer.setMensajeError("Unexpected error, try again a few minutes");
			}
			
			
			return responseTransfer;
	 }
	

}
