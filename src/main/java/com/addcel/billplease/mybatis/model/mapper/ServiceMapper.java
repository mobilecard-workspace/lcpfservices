package com.addcel.billplease.mybatis.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.billplease.client.model.AccountRecord;
import com.addcel.billplease.mybatis.model.vo.AfiliacionVO;
import com.addcel.billplease.mybatis.model.vo.Bank;
import com.addcel.billplease.mybatis.model.vo.BankCodes;
import com.addcel.billplease.mybatis.model.vo.Card;
import com.addcel.billplease.mybatis.model.vo.CuentasEstablecimiento;
import com.addcel.billplease.mybatis.model.vo.Establecimiento;
import com.addcel.billplease.mybatis.model.vo.FromAccount;
import com.addcel.billplease.mybatis.model.vo.Operador;
import com.addcel.billplease.mybatis.model.vo.ProjectMC;
import com.addcel.billplease.mybatis.model.vo.Proveedor;
import com.addcel.billplease.mybatis.model.vo.TBitacoraBanorte;
import com.addcel.billplease.mybatis.model.vo.TBitacoraVO;
import com.addcel.billplease.mybatis.model.vo.User;




public interface ServiceMapper {
	
	public Establecimiento getEstablecimiento( @Param(value = "id_est") long id_est);
	public Establecimiento getEstablecimientoByUser(@Param(value = "admin") String admin);
	public void insertEstablecimiento(Establecimiento estab);
	public Establecimiento loginEstablecimiento(@Param(value = "user")String user,@Param(value = "pass")String pass);
	public Establecimiento loginEstablecimientoById(@Param(value = "idAdmin")long idAdmin,@Param(value = "pass")String pass);
	public List<Operador> getOperadores(long id_estab);
	public Operador getOperador(long id_oper);
	public int searchEstab(@Param(value = "usuario")String usuario);
	public void updateEstab(Establecimiento estab);
	public void estDelete(long id_usuario);
	public List<CuentasEstablecimiento> getCuentasEstablecimnientos(@Param(value = "id") long id);
	
	public void insertOperador(Operador oper);
	public Operador loginOperador(@Param(value = "user")String user,@Param(value = "pass")String pass);
	public Operador loginOperadorByEstablecimiento(@Param(value = "user")String user,@Param(value = "pass")String pass ,@Param(value = "idEst") long idEst);
	public int searchOper(@Param(value = "usuario")String usuario,@Param(value = "id_est") long id_est);
	public void updateOper(Operador operador);
	public void operDelete(@Param(value = "id_oper") long id_oper, @Param(value = "id_est") long id_est);
	
	public int verifyOper(@Param(value = "id_usuario") long id_usuario);
	public int verifyEsta(@Param(value = "id_usuario") long id_usuario);
	
	public int isBinValido(@Param(value = "bin")String bin);
	public String getParameter(@Param(value = "parameter") String parameter);
	public void userActivate(@Param(value = "iduser") long iduser);
	public List<BankCodes> getBankcodes();
	public Bank getBanco(@Param(value = "id_banco") long id_banco);
	public List<FromAccount> getEstablecimientos();
	public long getMaxIdBitacora();
	
	public void setOperador(@Param(value = "operador") long operador, @Param(value = "id_bitacora") long id_bitacora);
	
	public void addBitacora(TBitacoraVO bitacora);
	public void insertTbitacoraMCBanorte(TBitacoraBanorte bitacora);
	public Card getCard(@Param(value = "idUsuario") long idUsuario, @Param(value = "idTarjeta") long idTarjeta);
	public AccountRecord getAccount(@Param(value = "idaccount") long idaccount);
	public AfiliacionVO buscaAfiliacion(@Param(value = "banco")String banco);
	public TBitacoraBanorte getBitacoraBanorte(@Param(value = "idBitacora") long idBitacora);
	public void updateBitacoraBanorteOBJ(TBitacoraBanorte bitacora);
	public User getUserData(@Param(value = "idUsuario") long idUsuario);
	public void  updateTbitacora(@Param(value = "idBitacora") long idBitacora,@Param(value = "status") int status, @Param(value = "aut") String aut);
	//public String getParameter(@Param(value = "parameter") String parameter);
	public long getAccountId(@Param(value = "id_transaction") long id_transaction);
	
	public String Rules_LCPF(@Param(value = "idUsuario") long idUsuario, @Param(value = "bin") String bin, @Param(value = "idCard") long idCard,@Param(value = "idioma") String idioma);
	public Proveedor getProveedor(String proveedor);
	
	public void AdminPassUpdate(@Param(value = "idAdmin")long idAdmin, @Param(value = "NewPass")String NewPass, @Param(value = "status")int status); 
	public void AdminPassOperadorUpdate(@Param(value = "idAdmin")long idAdmin, @Param(value = "idOper")long idOper, @Param(value = "NewPass")String NewPass, @Param(value = "status")int status);
	public ProjectMC getProjectMC( @Param(value = "cont") String cont,@Param(value = "recu") String recu);
	public Double getComisionExtra(@Param(value = "monto_cuenta") double monto_cuenta, @Param(value = "monto_comision") double monto_comision, @Param(value = "monto_propina") double monto_propina, @Param(value = "id_usuario") long id_usuario );
}
