package com.addcel.billplease.mybatis.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Card {
	
	
	private long idtarjetasusuario;
	private int mobilecard; 
	private String idtarjetas_tipo;
	private String nombre_tarjeta;
	private String usrDomAmex;
 	private String usrCpAmex; 
 	private String ct;
 	private String vigencia;
 	private String numerotarjeta;
 	private int idfranquicia;
 	private int estado;
 	private String act_type;
 	private String clabe; 
 	private String num_cuenta;
 	private String idbanco;
 	
 	public Card() {
		// TODO Auto-generated constructor stub
	}
 	
 	public void setIdbanco(String idbanco) {
		this.idbanco = idbanco;
	}
 	
 	public String getIdbanco() {
		return idbanco;
	}
 	
	public String getAct_type() {
		return act_type;
	}

	public void setAct_type(String act_type) {
		this.act_type = act_type;
	}

	public String getClabe() {
		return clabe;
	}

	public void setClabe(String clabe) {
		this.clabe = clabe;
	}

	public String getNum_cuenta() {
		return num_cuenta;
	}

	public void setNum_cuenta(String num_cuenta) {
		this.num_cuenta = num_cuenta;
	}

	public long getIdtarjetasusuario() {
		return idtarjetasusuario;
	}

	public void setIdtarjetasusuario(long idtarjetasusuario) {
		this.idtarjetasusuario = idtarjetasusuario;
	}

	public int getMobilecard() {
		return mobilecard;
	}

	public void setMobilecard(int mobilecard) {
		this.mobilecard = mobilecard;
	}

	public String getIdtarjetas_tipo() {
		return idtarjetas_tipo;
	}

	public void setIdtarjetas_tipo(String idtarjetas_tipo) {
		this.idtarjetas_tipo = idtarjetas_tipo;
	}

	public String getNombre_tarjeta() {
		return nombre_tarjeta;
	}

	public void setNombre_tarjeta(String nombre_tarjeta) {
		this.nombre_tarjeta = nombre_tarjeta;
	}

	public String getUsrDomAmex() {
		return usrDomAmex;
	}

	public void setUsrDomAmex(String usrDomAmex) {
		this.usrDomAmex = usrDomAmex;
	}

	public String getUsrCpAmex() {
		return usrCpAmex;
	}

	public void setUsrCpAmex(String usrCpAmex) {
		this.usrCpAmex = usrCpAmex;
	}

	public String getCt() {
		return ct;
	}

	public void setCt(String ct) {
		this.ct = ct;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getNumerotarjeta() {
		return numerotarjeta;
	}

	public void setNumerotarjeta(String numerotarjeta) {
		this.numerotarjeta = numerotarjeta;
	}

	public int getIdfranquicia() {
		return idfranquicia;
	}

	public void setIdfranquicia(int idfranquicia) {
		this.idfranquicia = idfranquicia;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}
 	
 	
	
}
