package com.addcel.billplease.mybatis.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FromAccount {
	
	private long id;
	private String alias;
	private double comision_fija;
	private double comision_porcentaje;
	private String telefono;
	private String correo;
	private String urlLogo;
	
	public FromAccount() {
		// TODO Auto-generated constructor stub
	}
	
	public void setUrlLogo(String urlLogo) {
		this.urlLogo = urlLogo;
	}
	
	public String getUrlLogo() {
		return urlLogo;
	}
	
    public void setCorreo(String correo) {
		this.correo = correo;
	}
    
    public String getCorreo() {
		return correo;
	}
    
    public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
    
    public String getTelefono() {
		return telefono;
	}
    
	public double getComision_fija() {
		return comision_fija;
	}



	public void setComision_fija(double comision_fija) {
		this.comision_fija = comision_fija;
	}



	public double getComision_porcentaje() {
		return comision_porcentaje;
	}



	public void setComision_porcentaje(double comision_porcentaje) {
		this.comision_porcentaje = comision_porcentaje;
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}
	
	
	

}
