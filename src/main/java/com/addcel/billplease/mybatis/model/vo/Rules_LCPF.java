package com.addcel.billplease.mybatis.model.vo;

public class Rules_LCPF {
	
	private String messageError;
	private int errorCode;
	
	public Rules_LCPF() {
		// TODO Auto-generated constructor stub
	}
	
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	public int getErrorCode() {
		return errorCode;
	}
	
	public void setMessageError(String messageError) {
		this.messageError = messageError;
	}
	
	public String getMessageError() {
		return messageError;
	}
}
