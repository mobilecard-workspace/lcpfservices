package com.addcel.billplease.mybatis.model.vo;

public class User {

	 private long id_usuario;
	 private String eMail;
	 private String usr_telefono;
	 private String usr_nombre;
	 private int id_usr_status;
	 
	 public User() {
		// TODO Auto-generated constructor stub
	}
	 
	 public void setId_usr_status(int id_usr_status) {
		this.id_usr_status = id_usr_status;
	}
	 
	 public int getId_usr_status() {
		return id_usr_status;
	}

	public void setId_usuario(long id_usuario) {
		this.id_usuario = id_usuario;
	}
	
	public void setUsr_nombre(String usr_nombre) {
		this.usr_nombre = usr_nombre;
	}
	
	public String getUsr_nombre() {
		return usr_nombre;
	}
	
	public long getId_usuario() {
		return id_usuario;
	}
	 
	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getUsr_telefono() {
		return usr_telefono;
	}

	public void setUsr_telefono(String usr_telefono) {
		this.usr_telefono = usr_telefono;
	}
	 
	 
}
