package com.addcel.billplease.mybatis.model.vo;

public class CuentasEstablecimiento {

	private long id_bitacora;
	private String referencia_negocio; 
	private String codigo_aut; 
	private String fecha; 
	private double importe; 
	private double propina; 
	private double comision;
	private double total;
	
	public CuentasEstablecimiento() {
		// TODO Auto-generated constructor stub
	}

	public long getId_bitacora() {
		return id_bitacora;
	}

	public void setId_bitacora(long id_bitacora) {
		this.id_bitacora = id_bitacora;
	}

	public String getReferencia_negocio() {
		return referencia_negocio;
	}

	public void setReferencia_negocio(String referencia_negocio) {
		this.referencia_negocio = referencia_negocio;
	}

	public String getCodigo_aut() {
		return codigo_aut;
	}

	public void setCodigo_aut(String codigo_aut) {
		this.codigo_aut = codigo_aut;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}

	public double getPropina() {
		return propina;
	}

	public void setPropina(double propina) {
		this.propina = propina;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}
	
	
}
