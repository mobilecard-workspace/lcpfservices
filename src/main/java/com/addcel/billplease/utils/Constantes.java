package com.addcel.billplease.utils;

import java.util.HashMap;

public class Constantes {

	
	//Constantes PROSA
	  public static final String CURRENCY = "484"; //Moneda MXN
	  public static final String ADDRESS = "PROSA"; //domicilio del negocio
	  public static final String MERCHANT = "8039159"; // 7627488 8039159 afiliacion bancaria ABC Capital  
	  public static final String STORE = "1234"; //tienda
	  public static final String TERM = "001"; //terminal
	  public static final String URLBACK = "http://199.231.161.38:8080/MCH2Hpayment/comercio-con"; //url de respuesta
	  public static final String key="Xsw23Dsa3Adv2scDE";
	  public static final String  URLBACKPAY = "http://199.231.161.38:8081/apiPagoTerceros/payworks/respuesta";
	  public static final String URLBACK3DSPAYW  = "http://199.231.161.38:8081/MCH2HPayment/payworks/payworksRec3DRespuesta";
	  public static final String VAR_PAYW_URL_BACK_W2 = "http://199.231.161.38:8081/MCH2HPayment/payworks/payworks2RecRespuesta";
		public static final String urlSendmail = "http://192.168.75.53:8080/MailSenderAddcel/enviaCorreoAddcel";
		public static final String Path_images ="/usr/java/resources/images/Addcel/";
		
		
		//public static final String ACTIVATION_URL = "http://199.231.160.203:8089/LCPFServices/{idioma}/user/activate"; //QA
		//public static final String URL_LOGO = "http://199.231.160.203:8089/LCPFServices/logo_default.png"; //QA
		//public static final String ACTIVATION_URL = "https://mobilecard.mx:8444/LCPFServices/{idioma}/user/activate"; //PROD
		//public static final String URL_LOGO = "https://mobilecard.mx:8444/LCPFServices/logo_default.png"; //PROD
		
		
		
	  public static HashMap<String, String> errorPayW = new HashMap<String, String>();
		
		static{			
			errorPayW.put("200","Indica que la transacción es segura y se puede enviar a Payworks.");
			errorPayW.put("201","Se detecto un error general en el sistema de Visa o Master Card, favor de esperar unos momentos para reintentar la transacción.");
			errorPayW.put("421","El servicio 3D Secure no está disponible, favor de esperar unos momentos para reintentar la transacción.");
			errorPayW.put("422","Se produjo un problema genérico al momento de realizar la Autenticación.");
			errorPayW.put("423","La Autenticación de la Tarjeta no fue exitosa.");
			errorPayW.put("424","Autenticación 3D Secure no fue completada, no está ingresando correctamente la contraseña 3D Secure.");
			errorPayW.put("425","Autenticación Inválida, no está ingresando correctamente la contraseña3D Secure.");
			errorPayW.put("430","Tarjeta de Crédito nulo, la numero de Tarjeta se envió vacía.");
			errorPayW.put("431","Fecha de expiración nulo, la fecha de expricacion se envió vacía.");
			errorPayW.put("432","Monto nulo, la variable Total se envió vacía.");
			errorPayW.put("433","Id del comercio nulo, la variable MerchantId se envió vacía.");
			errorPayW.put("434","Liga de retorno nula, la variable ForwardPath se envió vacía.");
			errorPayW.put("435","Nombre del comercio nulo, la variable MerchantName se envió vacía.");
			errorPayW.put("436","Formato de TC incorrecto, la variable Card debe ser de 16 dígitos.");
			errorPayW.put("437","Formato de Fecha de Expiración incorrecto, la variable Expires debe tener el siguiente formato: YY/MM donde YY se refiere al año y MM se refiere al mes de vencimiento de la tarjeta.");
			errorPayW.put("438","Fecha de Expiración incorrecto, indica que el plástico esta vencido.");
			errorPayW.put("439","Monto incorrecto, la variable Total debe ser un número menor a 999,999,999,999.## con la fracción decimal opcional, esta debe ser a lo más de 2 décimas.");
			errorPayW.put("440","Formato de nombre del comercio incorrecto, debe ser una cadena de máximo 25 caracteres alfanuméricos.");
			errorPayW.put("441","Marca de Tarjeta nulo, la variable CardType se envió vacía.");
			errorPayW.put("442","Marca de Tarjeta incorrecta, debe ser uno de los siguientes valores: VISA (para tarjetas Visa) o MC (para tarjetas Master Card).");
			errorPayW.put("443","CardType incorrecto, se ha especificado el CardType como VISA, sin embargo, el Bin de la tarjeta indica que esta no es Visa.");
			errorPayW.put("444","CardType incorrecto, se ha especificado el CardType como MC, sin embargo, el Bin de la tarjeta indica que esta no es Master Card.");
			errorPayW.put("446","Monto incorrecto, la variable Total debe ser superior a 1.0 pesos.");
			errorPayW.put("498","Transacción expirada. Indica que la transacción sobre paso el límite de tiempo de respuesta esperado.");
			errorPayW.put("499","Usuario excedió en tiempo de respuesta. Indica que el usuario tardo en capturar la información de 3D Secure mayor al tiempo esperado.");
		}
	  
}
