package com.addcel.billplease.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

public class UtilsMC {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UtilsMC.class);
	 private static final String BASE = "0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	 
 public static void emailSend(String email, String Cadena, String wAsunto, String usuario, String url, String pass, String nombre, String[] cid ){
	   	 
	   	 // if (idi.equals("es")) {
	             if (patternMatches(email, ".*@(hotmail|live|msn|outlook)\\..*")) {
	               LOGGER.info("[ENVIANDO CORREO MICROSOFT]...");
	               SendMailMicrosoft(email, Cadena , wAsunto,
	                   usuario, url, pass, nombre,cid);
	             } else {
	               LOGGER.info("entro else ");
	               /*SendMailNormal(email, MessageType, subject, usuario,
	                   password);*/
	               SendMailMicrosoft(email, Cadena , wAsunto,
	                       usuario, url, pass, nombre,cid);
	             }
	             LOGGER.info("email enviado correctamente " + email);
	    
	     }
 
 public static boolean SendMailMicrosoft(String sMail, String Cadena, String wAsunto, String user,
	      String url, String pass, String nombre, String [] cid) {
	    boolean flag = false;
	   
	    try {

	      Cadena = Cadena.replaceAll("@NOMBRE", nombre);
	      Cadena = Cadena.replaceAll("@USUARIO", user);
	      Cadena = Cadena.replaceAll("@PASSWORD", pass);
	      Cadena = Cadena.replaceAll("@URL", url);
	      
	      MailSender correo = new MailSender();
	      String from = "no-reply@addcel.com";
	      String[] to = {sMail};
	      correo.setCc(new String[] {});
	      correo.setBody(Cadena);
	      correo.setFrom(from);
	      correo.setSubject(wAsunto);
	      correo.setTo(to);
	      correo.setCid(cid);
	      
	      Gson gson = new Gson();
	      String json = gson.toJson(correo);
	      LOGGER.info("antes de enviar mail");
	      MailSender(json);
	     // enviarCorreoMicrosoft(correo);
	      LOGGER.info("despues de enviar mail");
	      LOGGER.debug("mensaje hotmail enviado : " + sMail);
	    } catch (Exception ex) {
	      LOGGER.error("Ocurrio un error al enviar email hotmail {}:  {}", sMail, ex);
	    }
	    return flag;
	  }

public static void MailSender(String data) {
	String line = null;
	StringBuilder sb = new StringBuilder();
	try {
		LOGGER.info("Iniciando proceso de envio email Microsoft. ");
		URL url = new URL(Constantes.urlSendmail);
		LOGGER.info("Conectando con " + Constantes.urlSendmail);
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

		urlConnection.setDoOutput(true);
		urlConnection.setRequestProperty("Content-Type", "application/json");
		urlConnection.setRequestProperty("Accept", "application/json");
		urlConnection.setRequestMethod("POST");

		OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
		writter.write(data);
		writter.flush();

		LOGGER.info("Datos enviados, esperando respuesta");

		BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

		while ((line = reader.readLine()) != null) {
			sb.append(line);
		}

		LOGGER.info("Respuesta del servidor(envio email Microsoft) " + sb.toString());
	} catch (Exception ex) {
		LOGGER.error("Error en: sendMail, al enviar el email ", ex);
	}
}


public static boolean patternMatches(String email, String pattern){
	boolean flag = false;
	
	Pattern pat = Pattern.compile(pattern); //".*@(hotmail|live|msn|outlook)\\..*"
    Matcher mat = pat.matcher(email.toLowerCase());
    if (mat.matches()) {
   	 flag = true;
    }
	return flag;
}

public static String generatePassword(int longitud){
	StringBuffer contrasena = new StringBuffer();
	int numero = 0;
	for(int i = 0; i < longitud; i++){ //1
	    numero = (int)(Math.random()*(BASE.length()-1)); //2
	    contrasena.append(BASE.substring(numero, numero+1)); //4
	}
	return contrasena.toString();
}

}
